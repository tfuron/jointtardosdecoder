#ifndef RAREV_H
#define RAREV_H

#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <algorithm>
#include <cmath>
#include <tr1/random>

template <typename particle_t, typename f_t>
struct particle_score_t { 
    particle_t *x;
    double score;
    particle_score_t<particle_t,f_t>() : x(0), score(0.0) {}
    particle_score_t<particle_t,f_t>(f_t &f) {
        init(f);
    }
    void free(f_t &f) {
        f.free(x);
    }
    void init(f_t &f) {
        x = f.generate();
        score = f.score(x);
    }
    void copy(const particle_score_t &o, f_t &f) {
        f.copy(x, o.x);
        score = o.score;
    }
    void modify(const particle_score_t &o, f_t &f, double mu) {
        f.modify(x, o.x, mu);
        score = f.score(x);
    }
    inline bool operator<(const particle_score_t &o) const {
        return score > o.score; 
    }
};

template <typename particle_t, typename f_t>
double rarev_estimate_threshold(double threshold, f_t &f) {
    const unsigned int n = 250; // number of particles
    const unsigned int iter_max = 100000; 
    const unsigned int t = 10; // number of micro-replications
    const double mu_decay = 0.05; // decay rate for mu
    const double rate = 0.1; 
    const double a = 0.95; // geometric weight of the average
    
    if (debug) fprintf(stderr, "%s: generating...\n", progname);

    // Initial steps
    particle_score_t<particle_t,f_t> *X = new particle_score_t<particle_t,f_t>[n]; // matrix of generated vectors
    for (unsigned int i = 0; i < n; i++)
        X[i].init(f);
    std::make_heap(X, X+n);

    particle_score_t<particle_t,f_t> xtmp(f);

    unsigned int iter = 1; // number of iterations
    double rho = 1.0 - 1.0 / n;

    unsigned int trials = n; // count # of detection calls

    if (debug) fprintf(stderr, "%s: iterating...\n", progname);
    // Init of the sliding average and iterate 
    double prob = 0.0;
    double sum_accepted = 0.0;
    double sum_a = 0.0;
    unsigned int iter_a = 0;
    double mu = 1.0;
    while (X[0].score < threshold && iter < iter_max) {
        iter++;
        trials += t;

        // Remove minimum
        double min_score = X[0].score;
        std::pop_heap(X, X+n);

        // Draw a particle to be replicated
        unsigned int idx_rep = f.randi(n-1);
        X[n-1].copy(X[idx_rep], f);

        // Replication process
        unsigned int accepted = 0;
        for (unsigned int i = 0; i < t; i++) {
            // t consecutive micro-replications
            xtmp.modify(X[n-1], f, mu);
            if (xtmp.score > min_score) {
                std::swap(xtmp, X[n-1]);
                accepted++;
            }
        }
        
        // Integrate the new score
        std::push_heap(X, X+n);
        
        // Is the strength too big?
        sum_accepted = accepted + a * sum_accepted;
        sum_a += pow(a, iter_a);
        iter_a++;
        if (((sum_accepted / sum_a) <= (rate * t)) && iter_a > 10) {
            mu *= (1.0 - mu_decay);
            sum_accepted = 0.0;
            sum_a = 0.0;
            iter_a = 0;
        }
        
        if (debug && (iter % 1000) == 0) {
            fprintf(stderr, "%s:   %6d %e %e %.3f %d\n", progname, iter, X[0].score, pow(rho, iter - 1), mu, trials);
        }
    }

    // Conclude
    if (iter < iter_max) // Estimate probability
        prob = pow(rho, iter - 1);
    else // Failure, too few iterations
        prob = 0.0;
    
    // Cleanup
    xtmp.free(f);
    for (unsigned int i = 0; i < n; i++)
        X[i].free(f);
    delete [] X;
    
    return prob;
}

template <typename particle_t, typename f_t>
double rarev_estimate_quantile(double probability, f_t &f) {
    const unsigned int n = 250; // number of particles
    const unsigned int t = 10; // number of micro-replications
    const double mu_decay = 0.05; // decay rate of mu 
    const double rate = 0.1;
    const double a = 0.95; // geometric weight of the average
    //const double alpha = 0.95; // confidence interval
    const double La = sqrt(2.0) * 1.3859038243496775; // erfinv(alpha=0.95)
//    const double La = sqrt(2.0) * 1.6449763571331868; // erfinv(alpha=0.98)    
    double q = 0; 

    unsigned int iter_lower = floor(-1.0 * n * log(probability) - La*sqrt(-1.0 * n * log(probability)));
    unsigned int iter_estimate = ceil(log(probability) / log(1.0 - 1.0 / n));
    unsigned int iter_upper = ceil(-1.0 * n * log(probability) + La*sqrt(-1.0 * n * log(probability)));
    
    if (verbose) fprintf(stderr, "%s: target iterations: %d %d %d\n", progname, iter_lower, iter_estimate, iter_upper);
    
    if (debug) fprintf(stderr, "%s: generating...\n", progname);

    // Initial steps
    particle_score_t<particle_t,f_t> *X = new particle_score_t<particle_t,f_t>[n]; // matrix of generated vectors
    for (unsigned int i = 0; i < n; i++) 
        X[i].init(f);
    std::make_heap(X, X+n);

    particle_score_t<particle_t,f_t> xtmp(f);

    double rho = 1.0 - 1.0 / n;

    unsigned int trials = n; // count # of detection calls

    if (debug) fprintf(stderr, "%s: iterating...\n", progname);
    // Init of the sliding average and iterate 
    double sum_accepted = 0.0;
    double sum_a = 0.0;
    unsigned int iter_a = 0;
    double mu = 1.0;

    for (unsigned int iter = 2; iter <= iter_upper; iter++) {
        trials += t;

        // Remove minimum
        double min_score = X[0].score;
        std::pop_heap(X, X+n);

        // Draw a particle to be replicated
        unsigned int idx_rep = f.randi(n-1);
        X[n-1].copy(X[idx_rep], f);

        // Replication process
        unsigned int accepted = 0;
        for (unsigned int i = 0; i < t; i++) {
            // t consecutive micro-replications
            xtmp.modify(X[n-1], f, mu);
            if (xtmp.score > min_score) {
                std::swap(xtmp, X[n-1]);
                accepted++;
            }
        }
        
        // Integrate the new score
        std::push_heap(X, X+n);
        
        // Is the strength too big?
        sum_accepted = accepted + a * sum_accepted;
        sum_a += pow(a, iter_a);
        iter_a++;
        if (((sum_accepted / sum_a) <= (rate * t)) && iter_a > 10) {
            mu *= (1.0 - mu_decay);
            sum_accepted = 0.0;
            sum_a = 0.0;
            iter_a = 0;
        }
        
        if (debug && (iter % 1000) == 0) {
            fprintf(stderr, "%s:   %6d %e %e %.3f %d\n", progname, iter, X[0].score, pow(rho, iter - 1), mu, trials);
        }
        
        if (iter == iter_upper) {
            q = X[0].score;
            break;
        }
        
    }

    // Cleanup
    xtmp.free(f);
    for (unsigned int i = 0; i < n; i++)
        X[i].free(f);
    delete [] X;
    
    return q;
}

template <typename particle_t>
struct rarev_func_t {
    rarev_func_t(unsigned long seed = 1234) : prng(seed), randi(prng, uniform) {}
    double score(particle_t *x) const;
    particle_t *generate();
    void modify(particle_t *y, const particle_t *x, double mu);
    void copy(particle_t *y, const particle_t *x);
    void free(particle_t *x);

    std::tr1::mt19937 prng;
    std::tr1::uniform_int<unsigned int> uniform;
    std::tr1::variate_generator<std::tr1::mt19937, std::tr1::uniform_int<unsigned int> > randi;
};

#endif 
