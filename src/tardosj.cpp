/*
Copyright 2011 Peter Meerwald, Teddy Furon
and INRIA Rennes

This software is a computer program whose purpose is to construct
and decode fingerprinting (traitor tracing) codes, in particular
Tardos codes.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software. You can use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and, more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cstring>
#include <ctime>
#include <cmath>
#include <algorithm>
#include <set>
#include <map>
#include <sys/time.h>
#include <unistd.h>
#include "getopt.h"

#include "common.h"
#include "generate.h"
#include "collude.h"
#include "scores.h"
#include "weights.h"

#include "scores_joint.h"
#include "sideinfo.h"
#include "threshold.h"
#include "accuse.h"
#include "timer.h"
#include "debug.h"
#include "prng.h"
#include "prune.h"
#include "collusion_model.h"

const char *progname = "tardosj";

static const unsigned int COLLUDERS_CMAX = -1;
static const unsigned int COLLUDERS_REAL = -2;
static const unsigned int DEFAULT_COLLUDERS = COLLUDERS_CMAX;
static const unsigned int DEFAULT_COLLUDER_START = 2;
static const unsigned int DEFAULT_COLLUDER_BOUND = 10;

static const char *colluders_str(unsigned int c) {
    static char buf[32];
    switch (c) {
        case COLLUDERS_CMAX: return "cmax";
        case COLLUDERS_REAL: return "real";
        case 0: return "???";
        default:
            sprintf(buf, "fixed (%d)", c);
            return buf;
    }
}

static void count_colluders_innocents(const Sideinfo &si, unsigned int d, unsigned int &ncolluder, unsigned int &ninnocent) {
    ncolluder = 0;
    ninnocent = 0;
    for (unsigned int i = 0; i < d; i++) {
        if (si.in(i)) ncolluder++;
    }
    ninnocent = si.size() - ncolluder;
}

static void print_results(const char *tag, unsigned int m, unsigned int d, unsigned long seed, unsigned int t, double pfa, double awgn, const Timer &timer, unsigned int niter, unsigned int ncolluder, unsigned int ninnocent) {
    printf("%s %s: %u colluders, %u innocents %s %s (seed %ld / %d, m = %u, c = %u, Pfa = %.2e, AWGN %.3f) niter = %d, time = %lf\n", progname, tag, 
        ncolluder, ninnocent, 
        ncolluder < (d-ninnocent) ? "FN" : "OK",
        ninnocent > 0 ? "FA" : "OK",            
        seed, t, m, d, pfa, awgn,
        niter, timer());
    fflush(stdout);
}

static void print_results(const char *tag, const Sideinfo &si, unsigned int m, unsigned int d, unsigned long seed, unsigned int t, double pfa, double awgn, const Timer &timer, unsigned int niter) {

    unsigned int ncolluder, ninnocent;
    count_colluders_innocents(si, d, ncolluder, ninnocent);

    print_results(tag, m, d, seed, t, pfa, awgn, timer, niter, ncolluder, ninnocent);
}

static score_index_t<1> *iterate_single(const double *p, const codeword_t * const *Xt, const double *y,  Collusion &coll, Sideinfo &si, score_index_t<1> *scores, unsigned int m, unsigned int n, unsigned int c, unsigned int d, unsigned long seed, unsigned int test, unsigned int &bestn_out, accusation_mode_t accusation_mode, double pfa, double awgn, unsigned int &total_niter, bool &accused) {

    accused = false;
    assert(si.size() <= c);
    if (c - si.size() < 1 || (accusation_mode == one && si.size() > 0)) return scores;

    Timer timer;

    unsigned int bestn = 0;
    unsigned int niter = 0;
    while (bestn_out != bestn && bestn_out > 0 && si.size()+1 <= c) {

        bestn = bestn_out;
        niter++;

        coll.refine(p, y, si, m);

        if (verbose) si.print();
        scores = accusation_scores(Xt, p, y, coll, scores, si, m, n, bestn);

        if (!scores || bestn == 0 || std::isinf(scores[0].score)) {
            fprintf(stderr, "%s: abort, must have accused wrong!\n", progname);
            break;
        }

        print_top_scores(scores, bestn, (c - si.size())); 
//        print_colluder_indices(scores, bestn, c);

        double thres = scores_threshold(p, y, coll, si, m, pfa / n);
        
        unsigned int nuser_above = 0;
        for (unsigned int i = 0; i < bestn; i++) {
            if (scores[i].score > thres) nuser_above++;
            else break;
        }
        fprintf(stderr, "%s: %d users above threshold %f, highest %d: %f\n", progname, nuser_above, thres, scores[0].index[0], scores[0].score);

        unsigned int toaccuse = (nuser_above+1)/2;
        if (accusation_mode == one) toaccuse = std::min(toaccuse, 1u);
        accused = accuse(Xt, scores, si, bestn, c, thres, toaccuse) > 0;

        scores = get_top_scores(scores, si, bestn, bestn_out);

        if (!accused || (accusation_mode == one && si.size() > 0)) break;
    }

    print_results("single+SI", si, m, d, seed, test, pfa, awgn, timer, niter);
    total_niter += niter;
    
    return scores;
}

static unsigned long long fac(unsigned long long x) {
    assert(x > 0);
    if (x == 1) return 1;
    return x * fac(x - 1);
}

static score_index_t<1> *iterate_joint(const double *p, const codeword_t * const *Xt, const double *y, Collusion &coll, Sideinfo &si, score_index_t<1> *scores, unsigned int m, unsigned int n, unsigned int c, unsigned int d, unsigned int t, unsigned long seed, unsigned int test, unsigned int &bestn_out, accusation_mode_t accusation_mode, double pfa, double awgn, unsigned int &total_niter, bool &accused) {

    accused = false;

    assert(si.size() <= c);
    if (c - si.size() < t || (accusation_mode == one && si.size() > 0)) return scores;
    prune5m(bestn_out, t);
    unsigned int bestn = 0;

    Timer timer;
    unsigned int niter = 0;

    while (bestn_out != bestn && bestn_out > 0 && si.size()+t <= c) {
        bestn = bestn_out;
        niter++;

        coll.refine(p, y, si, m);

        double e;        
        unsigned long long ntuples = n_choose_k(n-c, t);
        if (ntuples > 0) e = pfa / ntuples;
        else e = pfa / (pow(n-c, t) / fac(t));

        double thres = joint_scores_threshold(p, y, coll, si, m, t, e);

        scores = accusation_joint_scores(Xt, p, y, coll, scores, si, m, n, t, bestn, thres);
        print_top_scores(scores, bestn, (c - si.size()), true); 
//        print_colluder_indices(scores, bestn, c);

        if (bestn == 0 || !scores || std::isinf(scores[0].score)) {
            fprintf(stderr, "%s: abort, must have accused wrong or lost colluders!\n", progname);
            break;
        }

        unsigned int nuser_above = 0;
        for (unsigned int i = 0; i < bestn; i++) {
            if (scores[i].score > thres) {
                nuser_above++;
            }
            else break;
        }

        fprintf(stderr, "%s: %d users above threshold %f, highest %d: %f\n", progname, nuser_above, thres, scores[0].index[0], scores[0].score);

        unsigned int toaccuse = 0;
        if (nuser_above > 0) {
            score_index_t<1> *s = new score_index_t<1>[1];
            for (unsigned int j = 0; j < t; j++) {
                Sideinfo si2(si);
                for (unsigned int i = 0; i < t; i++) 
                    if (j != i) si2.add_user(Xt, scores[i].index[0]);

                Collusion coll2(coll);
                coll2.init(p, y, si2, m, c, d);
                double thres = scores_threshold(p, y, coll, si2, m, pfa / n);

                s[0].index[0] = scores[j].index[0];
                s = accusation_scores(Xt, p, y, coll, s, si2, m, 1, 1);

                if (s[0].score > thres) {
                    fprintf(stderr, "%s: user %d score above threshold: %f > %f\n", progname, s[0].index[0], s[0].score, thres);
                    toaccuse++;
                }
                else 
                    fprintf(stderr, "%s: user %d score below threshold: %f < %f\n", progname, s[0].index[0], s[0].score, thres);

                if (s[0].score <= thres)
                    break;
                    
                break; // make only one accusation
            }

            delete [] s;
        }

        if (accusation_mode == one) toaccuse = std::min(toaccuse, 1u);
        accused = accuse(Xt, scores, si, bestn, c, thres, toaccuse) > 0;
        
        scores = get_top_scores(scores, si, bestn, bestn_out);

        if (!accused || (accusation_mode == one && si.size() > 0)) break;
        if (accused) break;
    }

    if (accused) {
        char tag[32];
        sprintf(tag, "joint%d", t);
        print_results(tag, si, m, d, seed, test, pfa, awgn, timer, niter);
    }
    total_niter += niter;

    return scores;
}   

static void usage() {
    fprintf(stderr, 
        "USAGE: %s [OPTIONS]\n\n"
        "  -n n\t\tnumber of users / codewords\n"
        "  -m n\t\tcode length (must be multiple of %d)\n"
        "  -c n\t\tcolluder assumption (default: %s)\n"
        "  -b n\t\tcolluders to test (default: %d)\n"
        "  -B n\t\tbound of colluders (default: %d)\n"
        "  -I n\t\tincrement for number of colluders to test (default: 1)\n"
        "  -C name\tcollusion strategy (default: uniform)\n"
        "\t\tcan be: uniform, majority, minority, allone, allzero,\n"
        "\t\t\twca-s, wca-j, averaging, minmax, random, coinflip\n"
        "  -M name\taccusation mode (default: one)\n"
        "\t\tcan be: one, many\n"
        "  -1\t\trestrict to single decoder only, no joint decoding\n"
        "  -e\t\testimate collusion model (default: known)\n"
        "  -j\t\tdisable joint decoding (default: joint)\n"
        "  -D name\tdecoder to use (default: hard)\n"
        "\t\tcan be: hard, soft, sweet, choose\n"
        "  -A n\t\tAWGN (default: 0.0)\n"
        "  -F n\t\tbias distribution (default: 0)\n"
        "\t\tcan be: 0 (Tardos), or 1 - 8 (discrete)\n"
        "  -s n\t\tpseudo-random number generator seed (default: 1234)\n"
        "  -P n\t\ttarget overall false-alarm rate (default: 1e-3)\n"
        "  -t n\t\tnumber of tests to perform per setting (default: 1)\n"
        "  -v\t\tturn verbose output on\n"
        "  -d\t\tturn debug output on\n"
        "  -T\t\tturn timing output on\n"
        "  -h\t\tdisplay this help and exit\n\n"
        "Compute accusation scores for colluded Tardos codewords.\n"
        "The collusion is randomly computed from the first n users;\n"
        "use -b and -B to specify the range of colluders to test.\n\n"
        "Example:\n"
        "  %s -v -m 512 -n 100 -I 0\n\n",
        progname, codeword_bits, 
        colluders_str(DEFAULT_COLLUDERS), DEFAULT_COLLUDER_START, DEFAULT_COLLUDER_BOUND,
        progname);
}

int main(int argc, char *argv[]) {
    int opt;

    assert(sizeof(u8) == 1 && sizeof(codeword_t)*8 == codeword_bits && codeword_bits == __WORDSIZE);

    long int seed = 1234;

    unsigned int n = 0; // number of users
    unsigned int m = 0; // code length
    unsigned int c_mode = DEFAULT_COLLUDERS; // initial number of colluders
    unsigned int c_start = DEFAULT_COLLUDER_START; // number of colluders to test
    unsigned int c_max = DEFAULT_COLLUDER_BOUND; // max. number of colluders (bound)
    double ber = 0.0; // BER for BSC
    double awgn = 0.0; // AWGN variance
    bool estimate_model = false; // collusion model estimation
    double pfa = 1e-3; // target overall false-alarm rate
    collusion_model_t collusion_model = uniform;
    accusation_mode_t accusation_mode = one;
    unsigned int tests = 1; // number of tests
    decoding_t decoding = hard; // choose (hard) decoder to use
    bool single_only = false; // only run single decoder
    unsigned int c_incr = 1; // increment for colluders to test
    unsigned int bias = 0; // bias distribution, 0..Tardos, or discrete
    bool disable_joint = false;
 
    while ((opt = getopt(argc, argv, "n:m:b:B:A:c:C:ejE:F:s:D:t:1I:TP:M:dvh?")) != EOF) {
        switch (opt) {
            case 'n': // number of users
                n = atoi(optarg);
                break;
            case 'm': // code length
                m = atoi(optarg);
//                if ((m % codeword_bits) != 0) {
//                    fprintf(stderr, "%s: code length (m) must be multiple of %d, exit.\n", progname, codeword_bits);
//                    exit(EXIT_FAILURE);
//                }
                break;
            case 'c': 
                if (!strcmp(optarg, "real")) c_mode = COLLUDERS_REAL;
                else if (!strcmp(optarg, "cmax")) c_mode = COLLUDERS_CMAX;
                else c_mode = atoi(optarg);
                if (c_mode == 0) {
                    fprintf(stderr, "%s: invalid colluder assumption %s, exit.\n", progname, optarg);
                    exit(EXIT_FAILURE);
                }
                break;
            case 'b': // number of colluders to test
                c_start = atoi(optarg);
                break;
            case 'B': // maximum number of colluders assumed in decoding
                c_max = atoi(optarg);
                break;
            case 'E': // BER of BSC
                ber = atof(optarg);
                if (ber < 0.0 || ber > 0.5) {
                    fprintf(stderr, "%s: BER %f out of range, exit.\n", progname, ber);
                    exit(EXIT_FAILURE);
                }
                break;
            case 'A': // AWGN channel variance, hard thresholding
                awgn = atof(optarg);
                if (awgn < 0.0) {
                    fprintf(stderr, "%s: AWGN %f out of range, exit.\n", progname, awgn);
                    exit(EXIT_FAILURE);
                }
                break;
            case 'e': // estimate collusion model
                estimate_model = true;
                break;
            case 'C': // collusion strategy
                if (!strcmp(optarg, "uniform")) collusion_model = uniform;
                else if (!strcmp(optarg, "majority")) collusion_model = majority;
                else if (!strcmp(optarg, "minority")) collusion_model = minority;
                else if (!strcmp(optarg, "averaging")) collusion_model = averaging;
                else if (!strcmp(optarg, "minmax")) collusion_model = minmax;
                else if (!strcmp(optarg, "random")) collusion_model = rnd;
                else if (!strcmp(optarg, "wca-s") || !strcmp(optarg, "wca")) collusion_model = wca_single;
                else if (!strcmp(optarg, "wca-j")) collusion_model = wca_joint;
                else if (!strcmp(optarg, "allone")) collusion_model = allone;
                else if (!strcmp(optarg, "allzero")) collusion_model = allzero;
                else if (!strcmp(optarg, "coinflip")) collusion_model = coinflip;
                else {
                    fprintf(stderr, "%s: unknown collusion strategy '%s', exit.\n", progname, optarg);
                    exit(EXIT_FAILURE);
                }
                break;
            case 'F': // bias distribution
                bias = atoi(optarg);
                break;
            case 'j': // disable joint decoding
                disable_joint = true;
                break;
            case 'M': // accusation mode
                if (!strcmp(optarg, "one")) accusation_mode = one;
                else if (!strcmp(optarg, "many")) accusation_mode = many;
                else {
                    fprintf(stderr, "%s: accusation mode '%s', exit.\n", progname, optarg);
                    exit(EXIT_FAILURE);
                }
                break;
            case 'P': // target overall false-alarm rate
                pfa = atof(optarg);
                if (pfa <= 0.0 || pfa >= 0.5) {
                    fprintf(stderr, "%s: target false-alarm rate %e out-of-range, exit.\n", progname, pfa);
                    exit(EXIT_FAILURE);
                }
                break;
            case 's': // RNG seed
                seed = atoi(optarg);
                break;
            case 'D': // decoding mode
                if (!strcmp(optarg, "hard")) decoding = hard;
                else if (!strcmp(optarg, "soft")) decoding = soft;
                else if (!strcmp(optarg, "sweet")) decoding = sweet;
                else if (!strcmp(optarg, "choose")) decoding = choose;
                else {
                    fprintf(stderr, "%s: unknown decoding mode '%s', exit.\n", progname, optarg);
                    exit(EXIT_FAILURE);
                }
                break;
            case 't': // number of tests to perform
                tests = atoi(optarg);
                if (tests > 100) {
                    fprintf(stderr, "%s: number of tests %d to big, exit.\n", progname, tests);
                }
                break;
            case '1': // only run single decoder
                single_only = true;
                break;
            case 'I': // increment of colluders to test
                c_incr = atoi(optarg);
                break;
            case 'd': // turn debug output on
                debug = true;
                break;
            case 'v': // turn verbose output on
                verbose = true;
                break;
            case 'T': // turn timing output on
                timing = true;
                break;
            case 'h': // usage help
            case '?':
                usage();
                exit(EXIT_SUCCESS);
        }
    }

    argc -= optind;
    argv += optind;

    // check parameters
    if (n == 0 || m == 0) {
        fprintf(stderr, "%s: m and n parameters not specified, exit.\n", progname);
        exit(EXIT_FAILURE);
    }

    if (decoding == choose && !estimate_model) {
        fprintf(stderr, "%s: estimation must be enabled to choose decoding model, exit.\n", progname);
        exit(EXIT_FAILURE);
    }

    if (c_start > c_max) {
        fprintf(stderr, "%s: number of colluders to test must be smaller than bound (%d <= %d), exit.\n", progname, c_start, c_max);
        exit(EXIT_FAILURE);
    }

    if (debug)
        fprintf(stderr, "%s: %d bit mode...\n", progname, codeword_bits);

    prng_init(seed);

    // assert(testcases());

    // generate secret and code sequences
    if (verbose)
        fprintf(stderr, "%s: allocating %lld Kbytes of memory\n", progname, (unsigned long long)n*m/8/1024);
    Timer timer;
    double *p;
    if (bias) {
        fprintf(stderr, "%s: generating secret (discrete %d)\n", progname, bias);
        p = generate_discrete_secret(m, bias);
    }
    else {
        fprintf(stderr, "%s: generating secret (Tardos)\n", progname);
        p = generate_secret(m);    
    }
    fprintf(stderr, "%s: generating sequences...\n", progname);
    codeword_t **X = generate_sequences(p, m, n + (tests-1) * c_max);
    if (verbose) fprintf(stderr, "%.3f sec\n", timer());

    print_secret(p, m);
    print_users(X, m, n);

    assert(m > 0 && n > 0);
    assert(c_max <= n);
    assert(c_start <= c_max);
    assert(c_mode != 0);
    assert(c_mode <= c_max || c_mode == COLLUDERS_REAL || c_mode == COLLUDERS_CMAX);
//    assert((m % codeword_bits) == 0);

    for (unsigned int t = 0; t < tests; t++) {
        codeword_t **Xt = &X[t * c_max];

        for (unsigned int c_coll = c_start; c_coll <= c_max; c_coll += c_incr) {    
            if (verbose) 
                fprintf(stderr, "%s: n = %d, m = %d, c = %d (%s), b = %d, BER %.2f, AWGN %.3f, Pfa %.2e, accuse %s, %s\n", 
                    progname, n, m, c_coll, collusion_model_str(collusion_model), c_max, 
                    ber, awgn, pfa, accusation_mode_str(accusation_mode),
                    decoding_str(decoding));
            if (timing)
                printf("%s: n = %d, m = %d, s = %ld, c = %d (%s), b = %d, BER %.2f, AWGN %.3f, Pfa %.2e, accuse %s, %s\n", 
                    progname, n, m, seed, c_coll, collusion_model_str(collusion_model), c_max, 
                    ber, awgn, pfa, accusation_mode_str(accusation_mode),
                    decoding_str(decoding));

            fprintf(stderr, "%s: computing collusion (%s)...\n", progname, 
                collusion_model_str(collusion_model));
            double *y = collude(Xt, c_coll, m, n, collusion_model, awgn);
            print_collusion(y, m, decoding);

		    // apply binary symmetric channel (BSC) 
		    // with given bit error rate (BER)
            if (ber > 0.0) {
                for (unsigned int i = 0; i < m; i++) {
                    double r = prng_double();
                    if (r < ber) {
                        // flip bit
                        y[i] = -y[i];
                    }
                }
            }

            if (decoding == hard) {
                // hard thresholding
                fprintf(stderr, "%s: hard thresholding...\n", progname);
                for (unsigned int i = 0; i < m; i++) {
                    y[i] = (y[i] > 0.0) ? 1.0 : -1.0;
                }
            }

            unsigned int c_test; 
            switch (c_mode) {
                case 0:
                    fprintf(stderr, "%s: invalid assumption on number of colluders, exit.\n", progname);
                    exit(EXIT_FAILURE);
                case COLLUDERS_CMAX:
                    c_test = c_max;
                    break;
                case COLLUDERS_REAL:
                    c_test = c_coll;  // knowning the number of colluders
                    break;
                default: 
                    c_test = c_mode;
                    break;
            }

            bool marking_assumption = (awgn == 0.0 && ber == 0.0 && 
                (collusion_model != averaging && collusion_model != minmax && collusion_model != rnd) && 
                decoding == hard) ? true : false;

            Timer total_timer;
            unsigned total_niter = 0;

            // sideinfo and collusion model
            Sideinfo si(m);
            Collusion coll(collusion_model, decoding, estimate_model, marking_assumption);
            score_index_t<1> *scores = 0;

            // iterative decoding
            while (si.size() < c_test && (accusation_mode != one || si.size() == 0)) {

                total_niter++;
                Timer timer;

                unsigned int bestn = std::min(n, 10000u);
                if (total_niter == 1) {
                    coll.init(p, y, si, m, c_test, c_max);
                    scores = accusation_scores(Xt, p, y, coll, m, n);
                }
                else {
                    coll.refine(p, y, si, m);
                    delete [] scores;
                    scores = accusation_scores(Xt, p, y, coll, si, m, n, bestn); 
                }

                print_top_scores(scores, bestn, (c_test - si.size())); 
//                print_colluder_indices(scores, bestn, c_test);

                double thres = scores_threshold(p, y, coll, si, m, pfa / n);

                unsigned int nuser_above = 0;
                for (unsigned int i = 0; i < bestn; i++) {
                    if (scores[i].score > thres) nuser_above++;
                    else break;
                }
                fprintf(stderr, "%s: %d users above threshold %f, highest %d: %f\n", progname, nuser_above, thres, scores[0].index[0], scores[0].score);


                if (total_niter == 1) {
                    unsigned ncolluder;
                    unsigned ninnocent;
                    count_colluders_innocents(si, c_coll, ncolluder, ninnocent);
                    for (unsigned int i = 0; i < std::min(bestn, c_test); i++) {
                        if (scores[i].score <= thres) break;
                        if (scores[i].index[0] < c_coll) ncolluder++;
                        else ninnocent++;
                        if (accusation_mode == one) break;
                    }
                    print_results("single+thres", m, c_coll, seed, t, pfa, awgn, timer, 1, ncolluder, ninnocent);
                }

                bool accused = false;
                unsigned int toaccuse = (nuser_above+1)/2;
                if (accusation_mode == one) toaccuse = std::min(toaccuse, 1u);
                accused = accuse(Xt, scores, si, bestn, c_test, thres, toaccuse) > 0;

                if (total_niter > 1 && accused) {
                    print_results("single+further", si, m, c_coll, seed, t, pfa, awgn, timer, total_niter);
                }

                unsigned int bestn_out = bestn;
                scores = get_top_scores(scores, si, bestn, bestn_out);

                if (single_only || (si.size() > 0 && accusation_mode == one))
                    break;

                if (accused && (total_niter == 1 || si.size() == (c_test - 1)))
                    scores = iterate_single(p, Xt, y, coll, si, scores, m, n, c_test, c_coll, seed, t,  bestn_out, accusation_mode, pfa, awgn, total_niter, accused);

                if (disable_joint)
                    break;

                scores = iterate_joint(p, Xt, y, coll, si, scores, m, n, c_test, c_coll, 2, seed, t, bestn_out, accusation_mode, pfa, awgn, total_niter, accused);
                if (accused) continue;

                scores = iterate_joint(p, Xt, y, coll, si, scores, m, n, c_test, c_coll, 3, seed, t, bestn_out, accusation_mode, pfa, awgn, total_niter, accused);
                if (accused) continue;

                scores = iterate_joint(p, Xt, y, coll, si, scores, m, n, c_test, c_coll, 4, seed, t, bestn_out, accusation_mode, pfa, awgn, total_niter, accused);
                if (accused) continue;

                scores = iterate_joint(p, Xt, y, coll, si, scores, m, n, c_test, c_coll, 5, seed, t, bestn_out, accusation_mode, pfa, awgn, total_niter, accused);
                if (accused) continue;

                scores = iterate_joint(p, Xt, y, coll, si, scores, m, n, c_test, c_coll, 6, seed, t, bestn_out, accusation_mode, pfa, awgn, total_niter, accused);
                if (accused) continue;

                scores = iterate_joint(p, Xt, y, coll, si, scores, m, n, c_test, c_coll, 7, seed, t, bestn_out, accusation_mode, pfa, awgn, total_niter, accused);
                if (accused) continue;

                scores = iterate_joint(p, Xt, y, coll, si, scores, m, n, c_test, c_coll, 8, seed, t, bestn_out, accusation_mode, pfa, awgn, total_niter, accused);
                if (accused) continue;
                
                // give up
                break;
            }

            print_results("total", si, m, c_coll, seed, t, pfa, awgn, total_timer, total_niter);

            delete [] scores;
            delete [] y;

            if (c_incr == 0) break;
        }    
    }

    free_sequences(X, n + (tests-1) * c_max);
    delete [] p;

    return EXIT_SUCCESS;
}
