#include "accuse.h"

const char *accusation_mode_str(accusation_mode_t mode) {
    switch (mode) {
        case one: return "one";
        case many: return "many";        
        default: return "???";
    }
}
