#include "timer.h"

Timer::Timer() {
    start();
}

void Timer::start() {
    gettimeofday(&timer[0], 0);
    timer[1].tv_sec = 0;
    timer[1].tv_usec = 0;    
}

void Timer::stop() {
    gettimeofday(&timer[1], 0);
}

double Timer::operator()() const {
    if (timer[1].tv_sec == 0 && timer[1].tv_usec == 0) {
        timeval t;
        gettimeofday(&t, 0);
        return diff(t, timer[0]);
    }
    else
        return diff(timer[1], timer[0]);    
}

double Timer::diff(const timeval &t1, const timeval &t0) {
    return (t1.tv_sec - t0.tv_sec) +
        (t1.tv_usec - t0.tv_usec) / 1e6;
}
