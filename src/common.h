#ifndef COMMON_H
#define COMMON_H

#include <cstdio>
#include <cmath>
#include <algorithm>
#include <set>
#include <stdint.h>
#include <cassert>
#include "timer.h"

extern const char *progname;
extern bool verbose;
extern bool debug;
extern bool quiet;
extern bool timing;

typedef unsigned char u8;

typedef unsigned long int codeword_t;
const unsigned int codeword_bits = __WORDSIZE;

template <unsigned int t = 1>
struct score_index_t {
    double score;
    unsigned int index[t];
    unsigned int power;
};

unsigned long long n_choose_k(unsigned int n, unsigned int k);
char *get_user_bits(const codeword_t * const *X, unsigned int m, unsigned int user);

template <typename T>
struct argcomp {
    argcomp(T *_data) : data(_data) {}
    bool operator()(unsigned int i, unsigned int j) { return (data[i] > data[j]); }
    const T *data;
};

template <typename T>
void argsort(T *data, unsigned int *indices, unsigned int n) {
    for (unsigned int i = 0; i < n; i++) {
        indices[i] = i;
    }
    
    argcomp<T> c(data);
    std::sort(indices, indices+n, c);
}

template <unsigned int t>
bool operator<(const score_index_t<t> &a, const score_index_t<t> &b) {
    return a.score > b.score;
}

template <unsigned int t>
void sort_scores(score_index_t<t> *scores, unsigned int n, unsigned int &nvalid) {
    if (verbose)
        fprintf(stderr, "%s: sorting %d results...\n", progname, n);

    Timer timer;
    nvalid = 0;
    for (unsigned int i = 0; i < n; i++) {
        int c = std::fpclassify(scores[i].score);
        if (c == FP_NAN) scores[i].score = -INFINITY;
        if (c == FP_NORMAL) nvalid++;
    }
    
    std::sort(scores, scores+n);
    timer.stop();
    if (timing) printf("%s: sorting %.3f sec, %d valid\n", progname, timer(), nvalid);
    if (verbose) fprintf(stderr, "%s: sorting %.3f sec, %d valid\n", progname, timer(), nvalid);
}

template <unsigned int t>
void sort_scores(score_index_t<t> *scores, unsigned int n) {
    unsigned int nvalid;
    sort_scores(scores, n, nvalid);
}

template <unsigned int t>
score_index_t<1> *get_top_indices(const score_index_t<t> *scores, unsigned int n, unsigned int &bestn) {
    score_index_t<1> *best_scores = new score_index_t<1>[bestn];
    std::set<unsigned int> index_seen;
    
    unsigned int k = 0;
    for (unsigned int i = 0; i < n; i++) {
        for (unsigned int j = 0; j < t; j++) {
            if (index_seen.find(scores[i].index[j]) == index_seen.end()) {
                // index not seen yet
                
                // add to best scores
                best_scores[k].score = scores[i].score;
                best_scores[k].index[0] = scores[i].index[j];
                k++;
                
                // add to indices already seen
                index_seen.insert(scores[i].index[j]);
                
                if (k >= bestn) goto done;
            }
        }
    }
    
    // we got fewer indices than asked for, adjust bestn
    bestn = k;
done:    
    return best_scores;
}

template <typename T>
inline T sqr(const T x) {
    // compute square of x
    return x * x;
}

inline unsigned int roundup(unsigned int m, unsigned int a) {
    // a must be power of 2
    return (m + a - 1) & ~(a - 1);
}

template <typename T>
inline T median(T a, T b, T c) {
    if (a > b) std::swap(a, b);
    if (b > c) std::swap(b, c);
    if (a > b) std::swap(a, b);
    return b;
}

unsigned int valid_users(const score_index_t<1>* scores, unsigned int n);

#endif
