#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include "generate.h"
#include "prng.h"

double *generate_secret(unsigned int m, double t) {
    // compute m secret Tardos probabilities p with cutoff t
    // input
    //   m .. code length
    //   t .. cutoff parameter
    // output
    //   vector of length m with Tardos probabilities 
    double t2 = asin(sqrt(t));
    double *p = new double[roundup(m, codeword_bits)];
    memset(p, 0, roundup(m, codeword_bits)*sizeof(double));

    for (unsigned int i = 0; i < m; i++) {
        double r = t2 + (M_PI_2 - 2*t2) * prng_double();
        p[i] = sqr(sin(r));
    }
    
    return p;
}

double *generate_discrete_secret(unsigned int m, unsigned int c) {
    double *p = new double[roundup(m, codeword_bits)];
    memset(p, 0, roundup(m, codeword_bits)*sizeof(double));

    for (unsigned int i = 0; i < m; i++) {
        switch (c) {
            case 1: 
            case 2: 
                p[i] = 0.5; 
                break;
            case 3:
            case 4: {
                    double p2[] = {0.21132, 0.78868};
                    p[i] = p2[prng_uint32() % 2];
                    break;
                }
            case 5:
            case 6: {
                    double p3[] = {0.11270, 0.5, 0.88730};
                    p[i] = p3[prng_uint32() % 3];
                    break;
                }
            case 7:
            case 8: {
                    double p4[] = {0.06943, 0.33001, 0.66999, 0.93057};
                    p[i] = p4[prng_uint32() % 4];
                    break;
                }
            default: 
                fprintf(stderr, "%s: invalid number of colluders (%d) for discrete case, exit.\n", progname, c);
                exit(EXIT_FAILURE);
        }
    }
    
    return p;
}

static codeword_t *generate_sequence(const double *p, unsigned int m, dsfmt_t *rnd_state) {
    codeword_t *x = (codeword_t *)_mm_malloc(roundup(m, codeword_bits)*sizeof(codeword_t)/codeword_bits, 16);
    
    for (unsigned int i = 0; i < m; i += codeword_bits) {
        x[i/codeword_bits] = 0;
        for (unsigned int b = 0; b < codeword_bits; b++) {
            codeword_t bit = (prng_double(rnd_state) < p[i+b]) ? 1 : 0;
            x[i/codeword_bits] |= (bit << b);
        }
    }

    for (unsigned int i = m; i < roundup(m, codeword_bits); i++) 
        x[i/codeword_bits] &= ~((codeword_t)1 << (i % codeword_bits));

    return x;
}

codeword_t *generate_sequence(const double *p, unsigned int m) {
    return generate_sequence(p, m, &global_rnd_state);
}

codeword_t **generate_sequences(const double *p, unsigned int m, unsigned int n) {
    // generate n code sequences of length m
    // input
    //   p .. Tardos probabilities
    //   m .. code length
    //   n .. number of users
    // output
    //   matrix (n x m) of code symbols
    codeword_t **X = new codeword_t*[n];
    for (unsigned int i = 0; i < n; i++) 
        X[i] = generate_sequence(p, m);
    
    return X;
}

void free_sequences(codeword_t **X, unsigned int n) {
    // frees memory for code matrix
    for (unsigned int i = 0; i < n; i++)
        _mm_free(X[i]);
    delete [] X;
}

void free_sequence(codeword_t *x) {
    _mm_free(x);
}
