#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cstring>
#include <tr1/random>

#include "collude.h"
#include "weights.h"
#include "prng.h"
#include "fmath.hpp"

static double wca_theta_joint[9][11] = {
    {0.0, 0.5, 1.0}, // c = 2
    {0.0, 0.340, 0.660, 1.0}, // c = 3
    {0.0, 0.260, 0.5, 0.740, 1.0}, // c = 4
    {0.0, 0.209, 0.403, 0.597, 0.791, 1.0}, // c = 5
    {0.0, 0.176, 0.338, 0.5, 0.662, 0.824, 1.0}, // c = 6
    {0.0, 0.151, 0.291, 0.431, 0.569, 0.709, 0.849, 1.0}, // c = 7
    {0.0, 0.133, 0.256, 0.378, 0.5, 0.622, 0.744, 0.867, 1.0}, // c = 8
    {0.0, 0.119, 0.229, 0.338, 0.446, 0.554, 0.662, 0.771, 0.881, 1.0}, // c = 9
};

static double wca_theta_single[9][11] = {
    {0.0, 0.5, 1.0}, // c=2
    {0.0, 0.6511, 0.3489, 1.0}, // c=3
    {0.0, 0.4870, 0.5, 0.5130, 1.0}, // c=4
    {0.0, 0.5932, 0.0, 1.0, 0.4068, 1.0}, // c=5
    {0.0, 0.5014, 0.1749, 0.5000, 0.8251, 0.4986, 1.0}, // c=6
    {0.0, 0.4641, 0.1199, 0.6252, 0.3748, 0.8801, 0.5359, 1.0}, // c=7
    {0.0, 0.4695, 0.0, 0.6891, 0.5, 0.3109, 1.0, 0.5305, 1.0}, // c=8
    {0.0, 0.4453, 0.0, 0.6129, 0.4268, 0.5732, 0.3871, 1.0000, 0.5547, 1.0000}, // c=9
    {0.0, 0.3985, 0.0369, 0.5398, 0.3826, 0.5000, 0.6174, 0.4602, 0.9631, 0.6015, 1.0000}, // c=10
};

double *collude(const codeword_t * const *X, unsigned int c, unsigned int m, unsigned int n, collusion_model_t model, double awgn) {
    // compute sequence of code symbols; first c users are colluders
    // collusion strategy is 'uniform' at the moment
    // input
    //   X .. matrix (n x m) of user codes
    //   c .. number of colluders
    //   m .. code length
    //   n .. number of users
    // output
    //   vector of length m of code symbols

    assert(c <= n);
    assert(c >= 2);
    
    double *y = new double[m];

    switch (model) {
        case uniform:
            for (unsigned int i = 0; i < m; i++) {
                unsigned int assignation = prng_uint32() % c;
                assert(assignation < n);
                codeword_t bit = (X[assignation][i/codeword_bits] >> (i % codeword_bits)) & 1;
                y[i] = bit ? 1.0 : -1.0;
            }
            break;
        case majority:
            for (unsigned int i = 0; i < m; i++) {
                unsigned int k = 0;
                for (unsigned int j = 0; j < c; j++) 
                    if ((X[j][i/codeword_bits] >> (i % codeword_bits)) & 1) k++;
                if ((c & 1) == 0) {
                    if (k == c/2) y[i] = (prng_uint32() & 1) ? 1.0 : -1.0;
                    else y[i] = (k > c/2) ? 1.0 : -1.0;
                }
                else {
                    y[i] = (k > c/2) ? 1.0 : -1.0;
                }
            }
            break;
        case minority:
            for (unsigned int i = 0; i < m; i++) {
                unsigned int k = 0;
                for (unsigned int j = 0; j < c; j++) 
                    if ((X[j][i/codeword_bits] >> (i % codeword_bits)) & 1) k++;
                if (k == 0) y[i] = -1.0; 
                else if (k == c) y[i] = 1.0; 
                else {
                    if ((c & 1) == 0) {
                        if (k == c/2) y[i] = (prng_uint32() & 1) ? 1.0 : -1.0;
                        else y[i] = (k < c/2) ? 1.0 : -1.0;
                    }
                    else {
                        y[i] = (k <= c/2) ? 1.0 : -1.0;
                    }
                }
            }
            break;
        case averaging: {
            for (unsigned int i = 0; i < m; i++) {
                unsigned int k = 0;
                for (unsigned int j = 0; j < c; j++) 
                    if ((X[j][i/codeword_bits] >> (i % codeword_bits)) & 1) k++;

                y[i] = 2*k / (double) c - 1.0;
            }
            break;
        }
        case minmax: {
            for (unsigned int i = 0; i < m; i++) {
                unsigned int k = 0;
                for (unsigned int j = 0; j < c; j++) 
                    if ((X[j][i/codeword_bits] >> (i % codeword_bits)) & 1) k++;

                if (k == 0) y[i] = -1.0;
                else if (k == c) y[i] = 1.0;
                else y[i] = 0.0;
            }
            break;
        }
        case rnd: {
            for (unsigned int i = 0; i < m; i++) {
                unsigned int k = 0;
                for (unsigned int j = 0; j < c; j++) 
                    if ((X[j][i/codeword_bits] >> (i % codeword_bits)) & 1) k++;

                if (k == 0) y[i] = -1.0;
                else if (k == c) y[i] = 1.0;
                else y[i] = prng_double() * 2.0 - 1.0;
            }
            break;
        }
        case wca_single:
            if (c < 2 || c > 10) {
                fprintf(stderr, "%s: worst-case attack (single) for c=%d not implemented, exit.\n", progname, c);
                exit(EXIT_FAILURE);
            }
            for (unsigned int i = 0; i < m; i++) {
                unsigned int k = 0;
                for (unsigned int j = 0; j < c; j++) 
                    if ((X[j][i/codeword_bits] >> (i % codeword_bits)) & 1) k++;
                if (k == 0) y[i] = -1.0;
                else if (k == c) y[i] = 1.0;
                else {
                    double r = prng_double();
                    y[i] = (r < wca_theta_single[c-2][k]) ? 1.0 : -1.0;
                }
            }
            break;
        case wca_joint:
            if (c < 2 || c > 9) {
                fprintf(stderr, "%s: worst-case attack (joint) for c=%d not implemented, exit.\n", progname, c);
                exit(EXIT_FAILURE);
            }
            for (unsigned int i = 0; i < m; i++) {
                unsigned int k = 0;
                for (unsigned int j = 0; j < c; j++) 
                    if ((X[j][i/codeword_bits] >> (i % codeword_bits)) & 1) k++;
                if (k == 0) y[i] = -1.0;
                else if (k == c) y[i] = 1.0;
                else {
                    double r = prng_double();
                    y[i] = (r < wca_theta_joint[c-2][k]) ? 1.0 : -1.0;
                }
            }
            break;
        case allone:
            for (unsigned int i = 0; i < m; i++) {
                unsigned int k = 0;
                for (unsigned int j = 0; j < c; j++) 
                    if ((X[j][i/codeword_bits] >> (i % codeword_bits)) & 1) k++;
                if (k > 0) y[i] = 1.0;
                else y[i] = -1.0;
            }
            break;
        case allzero:
            for (unsigned int i = 0; i < m; i++) {
                unsigned int k = 0;
                for (unsigned int j = 0; j < c; j++) 
                    if ((X[j][i/codeword_bits] >> (i % codeword_bits)) & 1) k++;
                if (k < c) y[i] = -1.0;
                else y[i] = 1.0;
            }
            break;
        case coinflip:
            for (unsigned int i = 0; i < m; i++) {
                unsigned int k = 0;
                for (unsigned int j = 0; j < c; j++) 
                    if ((X[j][i/codeword_bits] >> (i % codeword_bits)) & 1) k++;
                if (k == 0) y[i] = -1.0;
                else if (k == c) y[i] = 1.0;
                else y[i] = (prng_uint32() & 1) ? 1.0 : -1.0;;
            }
            break;
        default:
            fprintf(stderr, "%s: collusion model not implemented, exit.\n", progname);
            exit(EXIT_FAILURE);                
    }
            
    if (awgn > 0.0) {
        if (verbose)
            fprintf(stderr, "%s: adding AWGN (s2 = %.2f)\n", progname, awgn);

        std::tr1::mt19937 prng(prng_uint32());
        std::tr1::normal_distribution<double> normal(0, sqrt(awgn));
        std::tr1::variate_generator<std::tr1::mt19937, std::tr1::normal_distribution<double> > randn(prng,normal);
        for (unsigned int i = 0; i < m; i++) {
            double noise = randn();
            y[i] += noise; // add noise
        }
    }

    return y;
} 

double *collusion_model_theta(unsigned int c, collusion_model_t model) {
    // compute collusion model
    // input
    //   c .. number of colluders
    // output
    //   vector of length c+1
    
    assert(c >= 2);
    double *theta = new double[c+1];
    
    switch (model) {
        case uniform:
            for (unsigned int i = 0; i <= c; i++)
                theta[i] = i / (double) c;
            break;
        case minority:
            theta[0] = 0.0;
            theta[c] = 1.0;
            for (unsigned int i = 1; i < c; i++) {
                if (c & 1) { // odd
                    theta[i] = (i > c/2) ? 0.0 : 1.0;
                }
                else { // even
                    if (i == c/2) theta[i] = 0.5;
                    else theta[i] = (i > c/2) ? 0.0 : 1.0;
                }            
            }
            break;
        case majority:
        case averaging:
            for (unsigned int i = 0; i <= c; i++) {
                if (c & 1) { // odd
                    theta[i] = (i > c/2) ? 1.0 : 0.0;
                }
                else { // even
                    if (i == c/2) theta[i] = 0.5;
                    else theta[i] = (i > c/2) ? 1.0 : 0.0;
                }            
            }
            break;
        case wca_single: // worst-case attack single decoder
            if (c < 2 || c > 10) {
                fprintf(stderr, "%s: worst-case attack (single) for c=%d not implemented, exit.\n", progname, c);
                exit(EXIT_FAILURE);
            }
            for (unsigned int i = 0; i <= c; i++) {
                theta[i] = wca_theta_single[c-2][i];
            }
            break;
        case wca_joint: // worst-case attack joint decoder
            if (c < 2 || c > 9) {
                fprintf(stderr, "%s: worst-case attack (joint) for c=%d not implemented, exit.\n", progname, c);
                exit(EXIT_FAILURE);
            }
            for (unsigned int i = 0; i <= c; i++) {
                theta[i] = wca_theta_joint[c-2][i];
            }
            break;
        case allone:
            for (unsigned int i = 0; i <= c; i++) {
                theta[i] = (i > 0) ? 1.0 : 0.0;
            }
            break; 
        case allzero:
            for (unsigned int i = 0; i <= c; i++) {
                theta[i] = (i < c) ? 0.0 : 1.0;
            }
            break; 
        case minmax:
        case coinflip:
        case rnd:
            theta[0] = 0.0;
            theta[c] = 1.0;
            for (unsigned int i = 1; i < c; i++) 
                theta[i] = 0.5;
            break;
        default:
            fprintf(stderr, "%s: collusion model not implemented, exit.\n", progname);
            exit(EXIT_FAILURE);                
    }
    
    return theta;
}

const char *collusion_model_str(collusion_model_t model) {
    switch (model) {
        case uniform: return "uniform";
        case majority: return "majority";
        case minority: return "minority";
        case averaging: return "averaging";
        case minmax: return "minmax";
        case rnd: return "random";
        case wca_single: return "worst-case-single)";                        
        case wca_joint: return "worst-case-joint";
        case allone: return "all-one";                        
        case allzero: return "all-zero";                        
        case coinflip: return "coin-flip";                        
        default: return "???";
    }
}

static float **arrange_ProbS(const double * const *ProbS, const double *y, unsigned int m, unsigned int c, unsigned int &nzeros) {
    float **aProbS = new float*[c+1];
    for (unsigned int i = 0; i <= c; i++) {
        aProbS[i] = (float *)_mm_malloc(sizeof(float) * roundup(m, 4), 16);
        memset(aProbS[i], 0, sizeof(float) * roundup(m, 4));
    }

    unsigned int index[2];
    index[0] = 0;
    index[1] = m-1;    
    for (unsigned int i = 0; i < m; i++) {
        unsigned int idx;
        if (y[i] > 0.0) idx = index[1]--;
        else idx = index[0]++;
        for (unsigned int j = 0; j <= c; j++) {
            aProbS[j][idx] = ProbS[j][i];
        }
    }

    nzeros = index[0];
    return aProbS;
}

static void free_ProbSf(float **ProbS, unsigned int c) {
    for (unsigned int i = 0; i <= c; i++) 
        _mm_free(ProbS[i]);
    delete [] ProbS;
}

static void update_PY(__m128 *PY, const __m128 *PY_max, const __m128 * const *ProbS, float ti_new, float ti_old, unsigned int m, unsigned int i) {
    __m128 t = _mm_set1_ps(ti_new - ti_old);
    for (unsigned int j = 0; j < m/4; j++)
        PY[j] = PY_max[j] + t * ProbS[i][j];
}

double refine_theta(double *theta_start, const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, bool ma) {
    double **ProbS = ProbS_coll(p, si, m, c);

    float *theta_max = new float[c+1];
    for (unsigned int i = 0; i <= c; i++)
        theta_max[i] = theta_start[i];
    
    unsigned int nzeros = 0;
    float **aProbSf = arrange_ProbS(ProbS, y, m, c, nzeros);
    free_ProbS(ProbS, c);

     __m128 PY[2][roundup(m, 4)/4];
    __m128 *PY_cur = PY[0];
    __m128 *PY_max = PY[1];
    theta_ProbS(PY_max, theta_max, (const __m128 * const *)aProbSf, m, c);    
    float lmax = likelihood_theta_approx(PY_max, m, nzeros);

    fprintf(stderr, "%s: refining collusion model (theta, %f)...\n", progname, lmax);
    Timer timer;
    unsigned int tries = 100;
    if (ma) {
        while (tries-- >= 1) {
            float beta = 0.002f * tries;
            for (unsigned int i = 1; i < c; i++) {
                float f = theta_max[i] + beta*(prng_double()-0.5f);      
                if (f > 1.0f) f = 2.0f - f; 
                if (f < 0.0f) f = -f;
                update_PY(PY_cur, PY_max, (const __m128 * const *)aProbSf, f, theta_max[i], m, i);

                float l = likelihood_theta_approx(PY_cur, m, nzeros);
                if (l > lmax) {
                    lmax = l;
                    theta_max[i] = f;
                    std::swap(PY_cur, PY_max);
                }
            }
        }
    }
    else {
        while (tries-- >= 1) {
            float beta = 0.002f * tries;
            for (unsigned int i = 0; i <= c; i++) {
                float f = theta_max[i] + beta*(prng_double()-0.5f);      
                if (f > 1.0f) f = 2.0f - f; 
                if (f < 0.0f) f = -f;
                update_PY(PY_cur, PY_max, (const __m128 * const *)aProbSf, f, theta_max[i], m, i);

                float l = likelihood_theta_approx(PY_cur, m, nzeros);
                if (l > lmax) {
                    lmax = l;
                    theta_max[i] = f;
                    std::swap(PY_cur, PY_max);
                }
            }
        }
    }
    if (timing) printf("%s: refining theta %f, %.3f sec\n", progname, lmax, timer());
    if (verbose) fprintf(stderr, "%s: %f, %.3f sec\n", progname, lmax, timer());

    for (unsigned int i = 0; i <= c; i++)
        theta_start[i] = theta_max[i];

    delete [] theta_max;
    free_ProbSf(aProbSf, c);

    return lmax;
}

double *collusion_model_theta(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, bool ma) {
    double l;
    return collusion_model_theta(p, y, si, m, c, l, ma);
}

double *collusion_model_theta(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &l, bool ma) {
    float *theta_max = new float[c+1];
    theta_max[0] = 0.0f;
    for (unsigned int i = 1; i < c; i++)
        theta_max[i] = 0.5f;
    theta_max[c] = 1.0f;    
    
    float lmax = -INFINITY;
    double **ProbS = ProbS_coll(p, si, m, c);
    
    unsigned int nzeros = 0;
    float **aProbSf = arrange_ProbS(ProbS, y, m, c, nzeros);
    free_ProbS(ProbS, c);

    __m128 PY[2][roundup(m, 4)/4];
    __m128 *PY_cur = PY[0];
    __m128 *PY_max = PY[1];
    theta_ProbS(PY_max, theta_max, (const __m128 * const *)aProbSf, m, c);    

    if (!quiet) fprintf(stderr, "%s: estimating collusion model (theta)...\n", progname);
    Timer timer;
    unsigned int tries = 100;
    if (ma) {
        while (tries-- >= 1) {
            float beta = 0.002f * tries;
            for (unsigned int i = 1; i < c; i++) {
                float f = theta_max[i] + beta*(prng_double()-0.5f);      
                if (f > 1.0f) f = 2.0f - f; 
                if (f < 0.0f) f = -f;
                update_PY(PY_cur, PY_max, (const __m128 * const *)aProbSf, f, theta_max[i], m, i);

                float l = likelihood_theta_approx(PY_cur, m, nzeros);
                if (l > lmax) {
                    lmax = l;
                    theta_max[i] = f;
                    std::swap(PY_cur, PY_max);
                }
            }
        }
    }
    else {
        while (tries-- >= 1) {
            float beta = 0.002f * tries;
            for (unsigned int i = 0; i <= c; i++) {
                float f = theta_max[i] + beta*(prng_double()-0.5f);      
                if (f > 1.0f) f = 2.0f - f; 
                if (f < 0.0f) f = -f;
                update_PY(PY_cur, PY_max, (const __m128 * const *)aProbSf, f, theta_max[i], m, i);

                float l = likelihood_theta_approx(PY_cur, m, nzeros);
                if (l > lmax) {
                    lmax = l;
                    theta_max[i] = f;
                    std::swap(PY_cur, PY_max);
                }
            }
        }
    }
    timer.stop();
    if (timing) printf("%s: theta initial %f, %.3f sec\n", progname, lmax, timer());
    if (verbose) fprintf(stderr, "%s: theta initial %f, %.3f sec\n", progname, lmax, timer());

    free_ProbSf(aProbSf, c);

    double *theta_est = new double[c+1];
    for (unsigned int i = 0; i <= c ; i++) 
        theta_est[i] = theta_max[i];

    delete [] theta_max;
    l = lmax;
    return theta_est;
}

void compare_theta_likelihood(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, collusion_model_t model, const double *theta_est) {
    if (!verbose) return;
    
    double **ProbS = ProbS_coll(p, si, m, c);
    
    double *theta_opt = collusion_model_theta(c, model);

    double lopt = likelihood_theta(theta_opt, ProbS, y, m, c);
    double lest = likelihood_theta(theta_est, ProbS, y, m, c);
 
    fprintf(stderr, "%s: collusion (%s), c = %d: opt %f, est %f\n", progname, 
        collusion_model_str(model), c, lopt, lest);
    
    free_ProbS(ProbS, c);   
    delete [] theta_opt;
}

double *collusion_model_mu(unsigned int c, collusion_model_t model) {
    // compute collusion model
    // input
    //   c .. number of colluders
    // output
    //   vector of length c+1
    
    assert(c >= 2);
    double *mu = new double[c+1];

    switch (model) {
        case majority:
            if (c & 1) {
                for (unsigned int i = 0; i <= c; i++) {
                    if (i <= c/2) mu[i] = -1.0;
                    else mu[i] = 1.0;
                }
            }
            else {
                for (unsigned int i = 0; i <= c; i++)  {
                    if (i < c/2) mu[i] = -1.0;
                    else if (i == c/2) mu[i] = 0.0;
                    else mu[i] = 1.0;
                }
            }
            break;
        case minority:
            mu[0] = -1.0;
            mu[c] = 1.0;
            if (c & 1) {
                for (unsigned int i = 1; i < c; i++) {
                    if (i <= c/2) mu[i] = 1.0;
                    else mu[i] = -1.0;
                }
            }
            else {
                for (unsigned int i = 1; i < c; i++)  {
                    if (i < c/2) mu[i] = 1.0;
                    else if (i == c/2) mu[i] = 0.0;
                    else mu[i] = -1.0;
                }
            }
            break;
        case uniform:
        case averaging:
            for (unsigned int i = 0; i <= c; i++) 
                mu[i] = -1.0 + 2.0 * i / (double) c;
            break;
        case wca_single: // worst-case attack
        case wca_joint: 
            fprintf(stderr, "%s: worst-case attack for c=%d not implemented, exit.", progname, c);
            exit(EXIT_FAILURE);
            break;
        case allone:
            for (unsigned int i = 0; i <= c; i++) {
                if (i >= 1) mu[i] = 1.0;
                else mu[i] = -1.0;
            }
            break; 
        case allzero:
            for (unsigned int i = 0; i <= c; i++) {
                if (i < c) mu[i] = -1.0;
                else mu[i] = 1.0;
            }
            break; 
        case minmax:
        case coinflip:
        case rnd:
            mu[0] = -1.0;
            mu[c] = 1.0;
            for (unsigned int i = 1; i < c; i++) {
                mu[i] = 0.0;
            }
            break;
        default:
            fprintf(stderr, "%s: collusion model not implemented, exit.\n", progname);
            exit(EXIT_FAILURE);                
    }
    
    return mu;
}

double *collusion_model_nu(unsigned int c, collusion_model_t model) {
    return collusion_model_theta(c, model);
}

double *collusion_model_mu(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn) {
    double l;
    return collusion_model_mu(p, y, si, m, c, awgn, l);
}

/*
static double *collusion_model_mu_slow(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn_est, double &l) {

    double **ProbS = ProbS_coll(p, si, m, c);

    double *mu = new double[c+1];
    double *mu_max = new double[c+1]; 
    double *mu_awgn_max = new double[c+1]; 
    double l_awgn_max = -INFINITY;

    fprintf(stderr, "%s: estimating collusion model (mu)...\n", progname);
    Timer timer;

    double awgn_max = 0.0;
    for (double awgn = 0.01; awgn <= 1.25; awgn += 0.1) {
        for (unsigned int i = 0; i <= c; i++) 
            mu[i] = mu_max[i] = 0.0;
        mu[0] = mu_max[0] = -1.0;
        mu[c] = mu_max[c] = 1.0;

        unsigned int tries = 100;
        double l_max = likelihood_mu_approx(mu_max, ProbS, y, m, c, awgn);
        while (tries-- > 0) {
            double beta = 0.002 * tries;
            for (unsigned int i = 0; i <= c; i++) {

                double f = mu[i] + beta*(prng_double() - 0.5);      
                if (f > 1.0) f = 2.0 - f; 
                else if (f < -1.0) f = -2.0 - f;
                
                mu[i] = f;
                
                double l = likelihood_mu_approx(mu, ProbS, y, m, c, awgn);
                if (l > l_max) {
                    l_max = l;
                    mu_max[i] = mu[i];
                }
                else {
                    mu[i] = mu_max[i];
                }
            }
        }       
        
        if (l_max > l_awgn_max) {
            l_awgn_max = l_max;
            for (unsigned int i = 0; i <= c; i++) 
                mu_awgn_max[i] = mu_max[i];
            awgn_max = awgn;
        } 
    }
    timer.stop();
    if (timing) printf("%s: mu initial %f, AWGN %.2f, %.3f sec\n", progname, l_awgn_max, awgn_max, timer());
    if (verbose) fprintf(stderr, "%s: mu initial %f, AWGN %.2f, %.3f sec\n", progname, l_awgn_max, awgn_max, timer());

    free_ProbS(ProbS, c);

    delete [] mu;
    delete [] mu_max;
    l = l_awgn_max;
    awgn_est = awgn_max;
    return mu_awgn_max;
}
*/

static float **conv_ProbS(const double * const *ProbS, unsigned int m, unsigned int c) {
    float **ProbSf = new float*[c+1];
    for (unsigned int i = 0; i <= c; i++) {
        ProbSf[i] = (float *)_mm_malloc(sizeof(float) * roundup(m, 4), 16);
        memset(ProbSf[i], 0, sizeof(float) * roundup(m, 4));
    }

    for (unsigned int i = 0; i < m; i++) {
        for (unsigned int j = 0; j <= c; j++) {
            ProbSf[j][i] = ProbS[j][i];
        }
    }

    return ProbSf;
}

static __m128 *conv_y(const double *y, unsigned int m) {
    float *yf = (float *)_mm_malloc(sizeof(float) * roundup(m, 4), 16);
    memset(yf, 0, sizeof(float) * roundup(m, 4));

    for (unsigned int i = 0; i < m; i++) 
        yf[i] = y[i];
    
    return (__m128 *)yf;
}

static void free_yf(__m128 *yf) {
    _mm_free(yf);
}

static inline float accum(__m128 &x) {
    return ((float *)&x)[0] + ((float *)&x)[1] + ((float *)&x)[2] + ((float *)&x)[3];
}

static void init_mu_PY(__m128 *PY, const float *mu, const __m128 *yf, const __m128 * const *ProbS, unsigned int m, unsigned int c, float awgn) {
    const __m128 f = _mm_set1_ps(-0.5f / awgn);
    const __m128 mu4 = _mm_set1_ps(mu[0]);
    const __m128 *ps = ProbS[0];
    for (unsigned int i = 0; i < m/4; i++) { 
        __m128 tmp = yf[i] - mu4;
        PY[i] = fmath::exp_ps(tmp * tmp * f) * ps[i];
    }

    for (unsigned int j = 1; j <= c; j++) {
        const __m128 mu4 = _mm_set1_ps(mu[j]);
        const __m128 *ps = ProbS[j];
        for (unsigned int i = 0; i < m/4; i++) { 
            __m128 tmp = yf[i] - mu4;
            PY[i] += fmath::exp_ps(tmp * tmp * f) * ps[i];
        }
    }
}

static void update_mu_PY(__m128 *PY, const __m128 *PY_max, const __m128 *ProbSi, const __m128 *yf, float mu_new, float mu_old, unsigned int m, float awgn) {
    const __m128 f = _mm_set1_ps(-0.5f / awgn);
    const __m128 mu4_new = _mm_set1_ps(mu_new);
    const __m128 mu4_old = _mm_set1_ps(mu_old);
    const __m128 *ps = ProbSi;
    
    for (unsigned int j = 0; j < m/4; j++) {
        __m128 t_old = yf[j] - mu4_old;
        __m128 t_new = yf[j] - mu4_new;
        __m128 t = fmath::exp_ps(t_new * t_new * f) - fmath::exp_ps(t_old * t_old * f);
        PY[j] = PY_max[j] + t * ps[j];
    }
}

static float likelihood_PY_approx(const __m128 *PY, unsigned int m, float awgn) {
    __m128 a = _mm_setzero_ps();
    const __m128 g = _mm_set1_ps(1.0f / sqrtf(2.0f * 3.1415926f * awgn));
    for (unsigned int i = 0; i < m/4; i++) 
        a += fmath::log_ps(PY[i] * g);

    return accum(a);
}

double *collusion_model_mu(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn_est, double &l) {

    double **ProbS = ProbS_coll(p, si, m, c);
    float **ProbSf = conv_ProbS(ProbS, m, c);
    free_ProbS(ProbS, c);

    __m128 *yf = conv_y(y, m);

    float *mu_max = new float[c+1]; 
    double *mu_awgn_max = new double[c+1]; 
    float l_awgn_max = -INFINITY;

    fprintf(stderr, "%s: estimating collusion model (mu)...\n", progname);
    Timer timer;

    float awgn_max = 0.0f;
    for (float awgn = 0.01f; awgn <= 1.55f; awgn += 0.1f) {
        for (unsigned int i = 1; i < c; i++) 
            mu_max[i] = 0.0f;
        mu_max[0] = -1.0f;
        mu_max[c] = 1.0f;

        __m128 PY[2][roundup(m, 4)/4];
        __m128 *PY_cur = PY[0];
        __m128 *PY_max = PY[1];

        init_mu_PY(PY_max, mu_max, yf, (const __m128 * const *)ProbSf, m, c, awgn);

        unsigned int tries = 100;
        float l_max = likelihood_PY_approx(PY_max, m, awgn);
        while (tries-- > 0) {
            float beta = 0.002f * tries;
            for (unsigned int i = 0; i <= c; i++) {

                float f = mu_max[i] + beta*(prng_double() - 0.5f);      
                if (f > 1.0f) f = 2.0f - f; 
                else if (f < -1.0f) f = -2.0f - f;
                
                update_mu_PY(PY_cur, PY_max, (const __m128 *)ProbSf[i], yf, f, mu_max[i], m, awgn);
                
                float l = likelihood_PY_approx(PY_cur, m, awgn);
                if (l > l_max) {
                    l_max = l;
                    mu_max[i] = f;
                    std::swap(PY_cur, PY_max);
                }
            }
        }       
        
        if (l_max > l_awgn_max) {
            l_awgn_max = l_max;
            for (unsigned int i = 0; i <= c; i++) 
                mu_awgn_max[i] = mu_max[i];
            awgn_max = awgn;
        } 
    }
    timer.stop();
    if (timing) printf("%s: mu initial %f, AWGN %.2f, %.3f sec\n", progname, l_awgn_max, awgn_max, timer());
    if (verbose) fprintf(stderr, "%s: mu initial %f, AWGN %.2f, %.3f sec\n", progname, l_awgn_max, awgn_max, timer());

    free_ProbSf(ProbSf, c);
    free_yf(yf);

    delete [] mu_max;
    l = l_awgn_max;
    awgn_est = awgn_max;
    return mu_awgn_max;
}

double *collusion_model_nu(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn) {
    double l;
    return collusion_model_nu(p, y, si, m, c, awgn, l);
}

/*
static double *collusion_model_nu_slow(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn_est, double &l) {

    double **ProbS = ProbS_coll(p, si, m, c);

    double *nu = new double[c+1];
    double *nu_max = new double[c+1]; 
    double *nu_awgn_max = new double[c+1]; 
    double l_awgn_max = -INFINITY;

    fprintf(stderr, "%s: estimating collusion model (nu)...\n", progname);
    Timer timer;

    double awgn_max = 0.0;
    for (double awgn = 0.01; awgn <= 1.25; awgn += 0.1) {
        for (unsigned int i = 0; i <= c; i++) 
            nu[i] = nu_max[i] = 0.0;
        nu[0] = nu_max[0] = 0.0;
        nu[c] = nu_max[c] = 1.0;

        unsigned int tries = 100;
        double l_max = likelihood_nu_approx(nu_max, ProbS, y, m, c, awgn);
        while (tries-- > 0) {
            double beta = 0.002 * tries;
            for (unsigned int i = 0; i <= c; i++) {

                double f = nu[i] + beta*(prng_double() - 0.5);      
                if (f > 1.0) f = 2.0 - f; 
                if (f < 0.0) f = -f;
                
                nu[i] = f;
                
                double l = likelihood_nu_approx(nu, ProbS, y, m, c, awgn);
                if (l > l_max) {
                    l_max = l;
                    nu_max[i] = nu[i];
                }
                else {
                    nu[i] = nu_max[i];
                }
            }
        }       
        
        if (l_max > l_awgn_max) {
            l_awgn_max = l_max;
            for (unsigned int i = 0; i <= c; i++) 
                nu_awgn_max[i] = nu_max[i];
            awgn_max = awgn;
        } 
    }
    timer.stop();
    if (timing) printf("%s: nu initial %f, AWGN %.2f, %.3f sec\n", progname, l_awgn_max, awgn_max, timer());
    if (verbose) fprintf(stderr, "%s: nu initial %f, AWGN %.2f, %.3f sec\n", progname, l_awgn_max, awgn_max, timer());

    free_ProbS(ProbS, c);

    delete [] nu;
    delete [] nu_max;
    l = l_awgn_max;
    awgn_est = awgn_max;
    return nu_awgn_max;
}
*/

static void init_nu_PY(__m128 *PY, const float *nu, const __m128 *yf1, const __m128 *yf2, const __m128 * const *ProbS, unsigned int m, unsigned int c) {
    const __m128 one4 = _mm_set1_ps(1.0f);
    const __m128 nu4 = _mm_set1_ps(nu[0]);
    const __m128 *ps = ProbS[0];
    for (unsigned int i = 0; i < m/4; i++) { 
        PY[i] = (nu4 * yf1[i]  + (one4 - nu4) * yf2[i]) * ps[i];
    }

    for (unsigned int j = 1; j <= c; j++) {
        const __m128 nu4 = _mm_set1_ps(nu[j]);
        const __m128 *ps = ProbS[j];
        for (unsigned int i = 0; i < m/4; i++) { 
            PY[i] += (nu4 * yf1[i]  + (one4 - nu4) * yf2[i]) * ps[i];
        }
    }
}

static void update_nu_PY(__m128 *PY, const __m128 *PY_max, const __m128 *ProbSi, const __m128 *yf1, const __m128 *yf2, float nu_new, float nu_old, unsigned int m) {
    const __m128 nu4_new = _mm_set1_ps(nu_new);
    const __m128 nu4_old = _mm_set1_ps(nu_old);
    const __m128 *ps = ProbSi;
    
    for (unsigned int j = 0; j < m/4; j++) {
        __m128 t = (nu4_new - nu4_old) * yf1[j] + (nu4_old - nu4_new) * yf2[j];
        PY[j] = PY_max[j] + t * ps[j];
    }
}

double *collusion_model_nu(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn_est, double &l) {

    double **ProbS = ProbS_coll(p, si, m, c);
    float **ProbSf = conv_ProbS(ProbS, m, c);
    free_ProbS(ProbS, c);

    __m128 *yf = conv_y(y, m);

    float *nu_max = new float[c+1]; 
    double *nu_awgn_max = new double[c+1]; 
    float l_awgn_max = -INFINITY;
    const __m128 one4 = _mm_set1_ps(1.0f);

    fprintf(stderr, "%s: estimating collusion model (nu)...\n", progname);
    Timer timer;

    float awgn_max = 0.0f;
    for (float awgn = 0.01f; awgn <= 1.55f; awgn += 0.1f) {
        for (unsigned int i = 1; i < c; i++) 
            nu_max[i] = 0.5f;
        nu_max[0] = 0.0f;
        nu_max[c] = 1.0f;

        __m128 PY[2][roundup(m, 4)/4];
        __m128 *PY_cur = PY[0];
        __m128 *PY_max = PY[1];

        __m128 yf1[roundup(m, 4)/4];
        __m128 yf2[roundup(m, 4)/4];
        
        const __m128 f = _mm_set1_ps(-0.5f / awgn);
        for (unsigned int i = 0; i < m/4; i++) {
            __m128 tmp;
            tmp = yf[i] - one4;
            yf1[i] = fmath::exp_ps(tmp * tmp * f);
            tmp = yf[i] + one4;
            yf2[i] = fmath::exp_ps(tmp * tmp * f);
        }

        init_nu_PY(PY_max, nu_max, yf1, yf2, (const __m128 * const *)ProbSf, m, c);

        unsigned int tries = 100;
        float l_max = likelihood_PY_approx(PY_max, m, awgn);
        while (tries-- > 0) {
            float beta = 0.002f * tries;
            for (unsigned int i = 0; i <= c; i++) {

                float f = nu_max[i] + beta*(prng_double() - 0.5f);      
                if (f > 1.0f) f = 2.0f - f; 
                if (f < 0.0f) f = -f;
                
                update_nu_PY(PY_cur, PY_max, (const __m128 *)ProbSf[i], yf1, yf2, f, nu_max[i], m);
                
                float l = likelihood_PY_approx(PY_cur, m, awgn);
                if (l > l_max) {
                    l_max = l;
                    nu_max[i] = f;
                    std::swap(PY_cur, PY_max);
                }
            }
        }       
        
        if (l_max > l_awgn_max) {
            l_awgn_max = l_max;
            for (unsigned int i = 0; i <= c; i++) 
                nu_awgn_max[i] = nu_max[i];
            awgn_max = awgn;
        } 
    }
    timer.stop();
    if (timing) printf("%s: nu initial %f, AWGN %.2f, %.3f sec\n", progname, l_awgn_max, awgn_max, timer());
    if (verbose) fprintf(stderr, "%s: nu initial %f, AWGN %.2f, %.3f sec\n", progname, l_awgn_max, awgn_max, timer());

    free_ProbSf(ProbSf, c);
    free_yf(yf);

    delete [] nu_max;
    l = l_awgn_max;
    awgn_est = awgn_max;
    return nu_awgn_max;
}

double refine_mu(double *mu_start, const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn) {
    double l;

    double *mu_upd = collusion_model_mu(p, y, si, m, c, awgn, l);
    for (unsigned int i = 0; i <= c; i++)
        mu_start[i] = mu_upd[i];
    delete [] mu_upd;
    
    return l;
}

double refine_nu(double *nu_start, const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn) {
    double l;

    double *nu_upd = collusion_model_nu(p, y, si, m, c, awgn, l);
    for (unsigned int i = 0; i <= c; i++)
        nu_start[i] = nu_upd[i];
    delete [] nu_upd;
    
    return l;
}

double *expand_theta(const double *theta, unsigned int c, unsigned int c_new) {
    assert(c_new > c);
 
    double *theta_tmp = new double[c_new+1];
    for (unsigned int i = 0; i <= c; i++)   
        theta_tmp[i] = theta[i];
    double *theta_new = new double[c_new+1];

    for (unsigned int k = 1; k <= (c_new - c); k++) {
        for (unsigned int i = 1; i < (c + k); i++) {
            theta_new[i] = (i*theta_tmp[i-1] + (c+k-i)*theta_tmp[i]) / (double) (c+k);
        }
        theta_new[c+k] = 1.0;
        for (unsigned int i = 0; i <= (c + k); i++)   
            theta_tmp[i] = theta_new[i];
    }
    
    delete [] theta_tmp;
    
    return theta_new;   
}
