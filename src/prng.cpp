#include "prng.h"

#define DSFMT_MEXP 19937
#define HAVE_SSE2
extern "C" {
#include "dSFMT.h"
#include "dSFMT.c"
}

dsfmt_t global_rnd_state;

void prng_init(unsigned int seed, dsfmt_t *rnd_state) {    
    dsfmt_init_gen_rand(rnd_state, seed);
} 

