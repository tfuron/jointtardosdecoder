#ifndef SCORES_H
#define SCORES_H

#include "common.h" 
#include "sideinfo.h"
#include "collusion_model.h"

static const unsigned int aggregate = 8;

double **aggregate_weights(const double * const *W, unsigned int m);
void free_w(double **w);

double accusation_score(const codeword_t *x, const double * const *w, unsigned int m);

score_index_t<1> *accusation_scores(const codeword_t * const *X, const double *p, const double *y, const Collusion &coll, unsigned int m, unsigned int n);

score_index_t<1> *accusation_scores(const codeword_t * const *X, const double *p, const double *y, const Collusion &coll, const Sideinfo &si, unsigned int m, unsigned int n, unsigned int &bestn);

score_index_t<1> *accusation_scores(const codeword_t * const *X, const double *p, const double *y, const Collusion &coll, const score_index_t<1> *best_scores, Sideinfo &si, unsigned int m, unsigned int n, unsigned int bestn);

#endif
