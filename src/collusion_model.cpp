#include <cstring>
#include "collusion_model.h"
#include "weights.h"
#include "debug.h"

const char *decoding_str(decoding_t mode) {
    switch (mode) {
        case soft: return "soft";
        case hard: return "hard";
        case sweet: return "sweet";
        case choose: return "choose";
        default: return "???";
    }
}

double *Collusion::copy_model_est() const {
    assert(model_est && model_params > 0);
    if (!model_est || model_params == 0) return 0;
    
    double *model_copy = new double[model_params];
    for (unsigned int i = 0; i < model_params; i++)
        model_copy[i] = model_est[i];
    
    return model_copy;
}

void Collusion::print(unsigned int o) const {
    assert(model_est && model_params > 0);
    if (!debug) return;

    fprintf(stderr, "%s: model = ", progname);
    for (unsigned int i = 0; i < std::min(o, model_params); i++)
        fprintf(stderr, "%.3f ", model_est[i]);
    fprintf(stderr, "\n");
}


void Collusion::refine(const double *p, const double *y, Sideinfo &si, unsigned int m) {
    assert(model_est && model_params > 0);
    
    if (!estimate) 
        return;

    if (!si.is_dirty())
        return;
        
    switch (decoding) {
        case soft:
            refine_mu(model_est, p, y, si, m, model_params-1, awgn_est);
            break;
        case sweet:
            refine_nu(model_est, p, y, si, m, model_params-1, awgn_est);
            break;
        case hard:
            refine_theta(model_est, p, y, si, m, model_params-1, marking_assumption);
            compare_theta_likelihood(p, y, si, m, model_params-1, collusion_model, model_est);
            break;
        case choose: {
            double awgn_mu = 0.0, awgn_nu = 0.0;
            
            double *model_copy_mu = copy_model_est();
            double l_mu = refine_mu(model_copy_mu, p, y, si, m, model_params-1, awgn_mu);
            double *model_copy_nu = copy_model_est();
            double l_nu = refine_nu(model_copy_nu, p, y, si, m, model_params-1, awgn_nu);
            
            if (l_mu > l_nu) {
                delete [] model_copy_nu;
                if (verbose) fprintf(stderr, "%s: choosing mu model (%f > %f), AWGN %.2f\n", 
                    progname, l_mu, l_nu, awgn_mu);
                model_est = model_copy_mu;
                use_decoding = soft;
            }
            else {
                delete [] model_copy_mu;
                if (verbose) fprintf(stderr, "%s: choosing nu model (%f > %f), AWGN %.2f\n", 
                    progname, l_nu, l_mu, awgn_nu);
                model_est = model_copy_nu;
                use_decoding = sweet;
            }
            break;
        }
        default:
            fprintf(stderr, "%s: unknown decoding method, exit.\n", progname);
            exit(EXIT_FAILURE);
    }
    print();
    
    si.reset_dirty();
}

void Collusion::init(const double *p, const double *y, Sideinfo &si, unsigned int m, unsigned int c, unsigned int d) {
    if (model_est)
        delete [] model_est;

    model_params = c+1;

    switch (decoding) {
        case soft:
            if (estimate) model_est = collusion_model_mu(p, y, si, m, c, awgn_est);
            else model_est = collusion_model_mu(c, collusion_model);
            use_decoding = soft;
            break;
        case sweet:
            if (estimate) model_est = collusion_model_nu(p, y, si, m, c, awgn_est);
            else model_est = collusion_model_nu(c, collusion_model);
            use_decoding = sweet;
            break;
        case choose: {
            double l_mu = -INFINITY, l_nu = -INFINITY;
            double awgn_mu = 0.0, awgn_nu = 0.0;
            double *model_est_mu = collusion_model_mu(p, y, si, m, c, awgn_mu, l_mu);
            double *model_est_nu = collusion_model_nu(p, y, si, m, c, awgn_nu, l_nu);
            
            if (l_mu > l_nu) {
                delete [] model_est_nu;
                if (verbose) fprintf(stderr, "%s: choosing mu model (%f > %f), AWGN %.2f\n", 
                    progname, l_mu, l_nu, awgn_mu);
                model_est = model_est_mu;
                awgn_est = awgn_mu;
                use_decoding = soft;
            }
            else {
                delete [] model_est_mu;
                if (verbose) fprintf(stderr, "%s: choosing nu model (%f > %f), AWGN %.2f\n", 
                    progname, l_nu, l_mu, awgn_nu);
                model_est = model_est_nu;
                awgn_est = awgn_nu;
                use_decoding = sweet;
            }
            break;
        }
        case hard:
            if (estimate) model_est = collusion_model_theta(p, y, si, m, c, marking_assumption);
            else {
                if (c > d) {
                    double *model_est_d = collusion_model_theta(d, collusion_model);
                    if (verbose) fprintf(stderr, "%s: expanding theta (%d -> %d)...\n", progname, d, c);
                    model_est = expand_theta(model_est_d, d, c);
                    delete [] model_est_d;
                }
                else 
                    model_est = collusion_model_theta(c, collusion_model);
            }
            use_decoding = hard;
            // compare_theta_likelihood(p, y, si, m, c, collusion_model, collusion_model_est);
            break;
        default:
            fprintf(stderr, "%s: unknown decoding method, exit.\n", progname);
            exit(EXIT_FAILURE);
    }
    print();
    
    si.reset_dirty();
}

double **Collusion::weights(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int t) const {

    double **W;
    
    switch (use_decoding) {
        case soft:
            if (verbose) fprintf(stderr, "%s: soft decoding...\n", progname);
            W = compute_weights_mu(p, y, model_est, si, m, model_params-1, t, awgn_est);
            break;
        case sweet:
            if (verbose) fprintf(stderr, "%s: sweet decoding...\n", progname);
            W = compute_weights_nu(p, y, model_est, si, m, model_params-1, t, awgn_est);
            break;
        case hard:
            if (verbose) fprintf(stderr, "%s: hard decoding...\n", progname);
            W = compute_weights_theta(p, y, model_est, si, m, model_params-1, t);
            if (weights_inf(W, m, t)) {
                free_W(W, t);

                if (verbose) fprintf(stderr, "%s: modifying theta...\n", progname);

                double *theta_mod = new double[model_params];
                memcpy(theta_mod, model_est, sizeof(double) * model_params);
                for (unsigned int i = 0; i < model_params; i++) {
                    if (theta_mod[i] <= 0.0) theta_mod[i] =      0.0000001;
                    else if (theta_mod[i] >= 1.0) theta_mod[i] = 0.9999999;
                }
                W = compute_weights_theta(p, y, theta_mod, si, m, model_params-1, t);
                assert(!weights_inf(W, t));

                delete [] theta_mod;
            }
            break;
        default:
            fprintf(stderr, "%s: unknown decoding method, exit.\n", progname);
            exit(EXIT_FAILURE);
    }

    assert(!weights_inf(W, t));

    return W;
}

