#ifndef DEBUG_H
#define DEBUG_H

#include "common.h"
#include "collusion_model.h"

void print_weights(const double * const *W, unsigned int m, unsigned int t = 1, unsigned int o = 10);
void print_collusion(const double *y, unsigned int m, decoding_t decoding = hard, unsigned int o = 20);
void print_secret(const double *p, unsigned int m, unsigned int o = 10);
void print_users(const codeword_t * const *X, unsigned int m, unsigned int n, unsigned int p = 10, unsigned o = 10);
void print_PY(const double *PY, unsigned int m, unsigned int o = 10);
void print_ProbS(const double * const *ps, unsigned int m, unsigned int c, unsigned int o = 10);


template <unsigned int t>
void print_colluder_indices(score_index_t<t> *scores, unsigned int n, unsigned int c) {
    std::set<unsigned int> index_seen;
    fprintf(stderr, "%s: colluder indices...", progname);
    for (unsigned int i = 0; i < n; i++) {
        for (unsigned int j = 0; j < t; j++) 
        	if (scores[i].index[j] < c) {
                if (index_seen.find(scores[i].index[j]) != index_seen.end()) continue;
        	
        		fprintf(stderr, "  %d(%d)", i, scores[i].index[j]);
                index_seen.insert(scores[i].index[j]);
    		}
    }
    fprintf(stderr, "\n");
}

template <unsigned int t>
static void print_top_scores(score_index_t<t> *scores, unsigned int n, unsigned int o = 10, bool power = false) {
    if (t == 1) fprintf(stderr, "%s: top %d/%d scores...\n", progname, std::min(n, o), n);
    else fprintf(stderr, "%s: top %d/%d %d-tuple scores...\n", progname, std::min(n,o), n, t);
    
    for (unsigned int i = 0; i < std::min(n, o); i++) {
        score_index_t<t> *p = &scores[i];
        fprintf(stderr, "  %.6f", p->score);
        for (unsigned int j = 0; j < t; j++) 
             fprintf(stderr, " %d", p->index[j]);
        if (power) fprintf(stderr, " : %d\n", p->power);
        else fprintf(stderr, "\n");
    }
}

template <unsigned int t>
static void print_top_indices(score_index_t<t> *scores, unsigned int n, unsigned int o = 10) {
    std::set<unsigned int> index_seen;
    fprintf(stderr, "%s: top %d indices...\n  ", progname, o);
    for (unsigned int i = 0; i < n; i++) {
        score_index_t<t> *p = &scores[i];
    
        for (unsigned int j = 0; j < t; j++) {
            if (index_seen.find(scores[i].index[j]) != index_seen.end()) continue;

            fprintf(stderr, "  %u %f\n", p->index[0], p->score);
            index_seen.insert(scores[i].index[j]);
            
            if (index_seen.size() == o) return;
        }
    }
}

#endif

