#ifndef SCORES_JOINT_H
#define SCORES_JOINT_H

#include "common.h" 
#include "sideinfo.h"
#include "collusion_model.h"

struct user_info_t {
    double score;
    unsigned int power;
    unsigned int user;

    user_info_t(double s, unsigned int p, unsigned int i) : score(s), power(p), user(i) {
    }

    bool operator<(const user_info_t &o) const {
        if (score > o.score) return true;
        if (score == o.score && power > o.power) return true;
        return false;
    }
}; 

score_index_t<1> *accusation_joint_scores(const codeword_t * const *X, const double *p, const double *y, const Collusion &coll, score_index_t<1> *scores, Sideinfo &si, unsigned int m, unsigned int n, unsigned int t, unsigned int &bestn, double thres);

double accusation_joint_score(const codeword_t * const *X, const double * const *W, unsigned int m, unsigned int t);


#endif
