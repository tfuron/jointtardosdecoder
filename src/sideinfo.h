#ifndef SIDEINFO_H
#define SIDEINFO_H

#include <set>
#include <cassert>
#include "common.h"

class Sideinfo {
    public:
        Sideinfo(unsigned int _m) : m(_m), bits(new int[roundup(_m, codeword_bits)]) {
            for (unsigned int i = 0; i < roundup(m, codeword_bits); i++)
                bits[i] = 0;
            dirty = false;
        }
        Sideinfo(const Sideinfo &o) : m(o.m), bits(new int[roundup(o.m, codeword_bits)]), indices(o.indices) {
            for (unsigned int i = 0; i < roundup(m, codeword_bits); i++)
                bits[i] = o.bits[i];
            dirty = false;
        }
        ~Sideinfo() {
            delete [] bits;
        }

        void print(unsigned int o = 20) const;
        void add_user(const codeword_t * const *X, unsigned int user);
        void remove_user(const codeword_t * const *X, unsigned int user);
        void add_user(const u8 *x, unsigned int user);
        unsigned int size() const {
            return indices.size();
        }
        unsigned int operator[](unsigned int i) const {
            assert(i < m);
            return bits[i];
        }
        bool in(unsigned int user) const;
        const std::set<unsigned int> &users() const {
            return indices;
        }
        bool is_dirty() const { 
            return dirty;
        }
        void reset_dirty() {
            dirty = false;
        }

    private:
        unsigned int m;
        int *bits;
        std::set<unsigned int> indices;
        bool dirty;
};

#endif
