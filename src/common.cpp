#include <cstdlib>
#include <cassert>
#include <unistd.h>

#include "common.h"

bool verbose = false;
bool debug = false;
bool timing = false;
bool quiet = false;

unsigned long long n_choose_k(unsigned int n, unsigned int k) {
    assert(n >= k);
        
    if (k > n/2)
        k = n - k;
    
    double accum = 1;
    for (unsigned int i=1; i <= k; i++)
        accum *= (n-k+i) / (double) i;

    return (unsigned long long) (accum + 0.5);
}

char *get_user_bits(const codeword_t * const *X, unsigned int m, unsigned int user) {
    char *s = new char[m+1];
    const codeword_t *Xuser = X[user];

    for (unsigned int p = 0; p < m; p += codeword_bits) {
        codeword_t codeword = *Xuser++;

        for (unsigned int b = 0; b < codeword_bits; b++) {
            s[p+b] = (codeword & 1) ? '1' : '0';
            codeword >>= 1;
        }
    }
    s[m] = '\0';
    
    return s;
}

unsigned int valid_users(const score_index_t<1>* scores, unsigned int n) {
    unsigned int nvalid = 0;
    for (unsigned int i = 0; i < n; i++) {
        if (!std::isinf(scores[i].score) && !std::isnan(scores[i].score))
            nvalid++;
        if (std::isnan(scores[i].score)) {
            fprintf(stderr, "XXX NaN at %d!!!\n", i);
            exit(EXIT_FAILURE);
        }
    }

    return nvalid;
}

