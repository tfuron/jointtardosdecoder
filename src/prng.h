#ifndef PRNG_H
#define PRNG_H

#define DSFMT_MEXP 19937
#define HAVE_SSE2
extern "C" {
#include "dSFMT.h"
}

extern dsfmt_t global_rnd_state;

void prng_init(unsigned int seed, dsfmt_t *rnd_state = &global_rnd_state);

inline double prng_double(dsfmt_t *rnd_state = &global_rnd_state) {
    return dsfmt_genrand_open_open(rnd_state);
}

inline unsigned int prng_uint32(dsfmt_t *rnd_state = &global_rnd_state) {
    return dsfmt_genrand_uint32(rnd_state);
}

#endif
