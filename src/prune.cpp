#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include "common.h"
#include "prune.h"

void prune5m(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 3000u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 301u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 103u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 58u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 41u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 33u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 29u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}


void prune10m(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 4473u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 393u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 126u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 68u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 47u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 37u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 32u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}


void prune25m(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 7072u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 532u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 158u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 81u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 54u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 42u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 35u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}

void prune50m(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 10000u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 670u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 188u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 92u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 60u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 46u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 38u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}

void prune100m(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 14143u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 845u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 223u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 106u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 67u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 50u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 41u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}

void prune250m(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 20000u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 1146u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 280u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 127u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 78u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 57u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 46u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}


void prune2m(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 2000u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 230u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 85u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 49u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 36u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 30u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 27u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}

void prune1m(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 1415u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 183u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 72u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 43u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 33u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 27u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 25u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}

void prune500k(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 1000u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 145u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 60u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 38u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 29u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 25u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 23u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}

void prune250k(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 708u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 115u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 51u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 33u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 26u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 23u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 21u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}

void prune100k(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 448u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 86u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 41u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 28u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 23u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 21u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 20u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}

void prune1g(unsigned int &bestn_out, unsigned int t) {
    switch (t) {
        case 2:
            bestn_out = std::min(bestn_out, 44722u);
            break;
        case 3:
            bestn_out = std::min(bestn_out, 1818u);
            break;
        case 4:
            bestn_out = std::min(bestn_out, 395u);
            break;
        case 5:
            bestn_out = std::min(bestn_out, 166u);
            break;
        case 6:
            bestn_out = std::min(bestn_out, 97u);
            break;
        case 7:
            bestn_out = std::min(bestn_out, 68u);
            break;
        case 8:
            bestn_out = std::min(bestn_out, 54u);
            break;
        default:
            fprintf(stderr, "%s: %d-tuple undefined, exit.\n", progname, t);
            exit(EXIT_FAILURE);
    }
}

