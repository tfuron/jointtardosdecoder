#ifndef WEIGHTS_H
#define WEIGHTS_H

#include "common.h"
#include "sideinfo.h"
#include "collusion_model.h"
#include "xmmintrin.h"

double **ProbS_coll(const double *p, const Sideinfo &si, unsigned int m, unsigned int c, unsigned int t = 0, unsigned int s = 0);
void free_ProbS(double **ProbS, unsigned int c);

template <typename T>
void theta_ProbS(T *PY, const T *theta, const T * const *ProbS, unsigned int m, unsigned int c) {
    for (unsigned int i = 0; i < m; i++) 
        PY[i] = theta[0] * ProbS[0][i];
    
    for (unsigned int i = 1; i <= c; i++) {
        for (unsigned int j = 0; j < m; j++) 
            PY[j] += theta[i] * ProbS[i][j];
    }
}

void theta_ProbS(__m128 *PY, const float *theta, const __m128 * const *ProbS, unsigned int m, unsigned int c);

double likelihood_theta(const double *theta, const double * const *PS, const double *y, unsigned int m, unsigned int c);
double likelihood_theta_approx(const double *theta, const double * const *ProbS, const double *y, unsigned int m, unsigned int c);
float likelihood_theta_approx(const float *theta, const __m128 * const *ProbS, unsigned int m, unsigned int c, unsigned int zeros);
float likelihood_theta_approx(const __m128 * PY, unsigned int m, unsigned int zeros);

double **compute_weights_theta(const double *p, const double *y, const double *theta, const Sideinfo &si, unsigned int m, unsigned int c, unsigned int t);
void free_W(double **W, unsigned int t = 1);

bool weights_inf(const double * const *W, unsigned int m, unsigned int t = 1);

static inline double mu_awgn(double mu_est, double y, double var) {
    return exp(-0.5 * sqr(y - mu_est) / var) / sqrt(2.0 * M_PI * var);
}

template <typename T>
void mu_awgn_ProbS(T *PY, const double *mu, const double *y, const T * const *ProbS, unsigned int m, unsigned int c, double awgn) {
    for (unsigned int i = 0; i < m; i++) 
        PY[i] = mu_awgn(mu[0], y[i], awgn) * ProbS[0][i];
    
    for (unsigned int i = 1; i <= c; i++) {
        for (unsigned int j = 0; j < m; j++) {
            PY[j] += mu_awgn(mu[i], y[j], awgn) * ProbS[i][j];
        }
    }
}

static inline double nu_awgn(double nu_est, double y, double var) {
    return (nu_est * exp(-0.5 * sqr(y - 1.0) / var) + (1.0-nu_est) * exp(-0.5 * sqr(y - -1.0) / var)) / sqrt(2.0 * M_PI * var);
}

template <typename T>
void nu_awgn_ProbS(T *PY, const double *nu, const double *y, const T * const *ProbS, unsigned int m, unsigned int c, double awgn) {
    for (unsigned int i = 0; i < m; i++) 
        PY[i] = nu_awgn(nu[0], y[i], awgn) * ProbS[0][i];
    
    for (unsigned int i = 1; i <= c; i++) {
        for (unsigned int j = 0; j < m; j++) {
            PY[j] += nu_awgn(nu[i], y[j], awgn) * ProbS[i][j];
        }
    }
}

double **compute_weights_mu(const double *p, const double *y, const double *mu, const Sideinfo &si, unsigned int m, unsigned int c, unsigned int t, double awgn);

double likelihood_mu(const double *mu, const double * const *ProbS, const double *y, unsigned int m, unsigned int c, double awgn);

double likelihood_mu_approx(const double *mu, const double * const *ProbS, const double *y, unsigned int m, unsigned int c, double awgn);

float likelihood_mu_approx(const float *mu, const __m128 * const *ProbS, const __m128 *y, unsigned int m, unsigned int c, float awgn);

double **compute_weights_nu(const double *p, const double *y, const double *nu, const Sideinfo &si, unsigned int m, unsigned int c, unsigned int t, double awgn);

double likelihood_nu(const double *nu, const double * const *ProbS, const double *y, unsigned int m, unsigned int c, double awgn);

double likelihood_nu_approx(const double *nu, const double * const *ProbS, const double *y, unsigned int m, unsigned int c, double awgn);

float likelihood_nu_approx(const float *nu, const __m128 * const *ProbS, const __m128 *y, unsigned int m, unsigned int c, float awgn);


#endif
