#ifndef COLLUDE_H
#define COLLUDE_H

#include "common.h"
#include "sideinfo.h"

typedef enum {
    uniform,
    majority,
    minority,
    averaging,
    minmax,    
    rnd,
    wca_single,
    wca_joint,
    allone,
    allzero,
    coinflip
} collusion_model_t;

double *collude(const codeword_t * const *X, unsigned int c, unsigned int m, unsigned int n, collusion_model_t model, double awgn = 0.0);

double *collusion_model_theta(unsigned int c, collusion_model_t model);

const char *collusion_model_str(collusion_model_t model);

double *collusion_model_theta(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, bool ma = true);
double *collusion_model_theta(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &l, bool ma = true);

void compare_theta_likelihood(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, collusion_model_t model, const double *theta_est);

double refine_theta(double *theta_start, const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, bool ma = true);

double *expand_theta(const double *theta, unsigned int c, unsigned int c_new);

double *collusion_model_mu(unsigned int c, collusion_model_t model);
double *collusion_model_mu(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn);
double *collusion_model_mu(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn, double &l);

double *collusion_model_nu(unsigned int c, collusion_model_t model);
double *collusion_model_nu(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn);
double *collusion_model_nu(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn, double &l);

double refine_mu(double *mu_start, const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn);

double refine_nu(double *nu_start, const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int c, double &awgn);

#endif
