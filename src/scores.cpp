#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cstring>
#include <cmath>
#include <algorithm>

#include "scores.h"
#include "weights.h"
#include "debug.h"

double **aggregate_weights(const double * const *W, unsigned int m) {
    double **w = new double*[1 << aggregate];

    for (unsigned int a = 0; a < (unsigned int) (1 << aggregate); a++) {
        w[a] = new double[roundup(m, codeword_bits)/aggregate];
        memset(w[a], 0, roundup(m, codeword_bits)*sizeof(double)/aggregate);
        for (unsigned int i = 0; i < m; i += aggregate) {
            for (unsigned int b = 0; b < aggregate; b++) {
                if (i+b < m) 
                    w[a][i/aggregate] += W[(a >> b) & 1][i+b];
            }
            if (std::isnan(w[a][i/aggregate])) {
                fprintf(stderr, "%s: NaN in aggregated weights, must not happen, exit!\n", progname);
                exit(EXIT_FAILURE);
            }
        }
    }
    
    return w;
}

void free_w(double **w) {
    for (unsigned int a = 0; a < (unsigned int) (1 << aggregate); a++) {
        delete [] w[a];
    }
    
    delete [] w;
}

double accusation_score(const codeword_t *x, const double * const *w, unsigned int m) {
    double s = 0.0;
    
    for (unsigned int j = 0; j < roundup(m, codeword_bits)/aggregate; ) {
        codeword_t bits = *x++;
        for (unsigned int k = 0; k < codeword_bits/aggregate/4; k++) {
            s += w[bits & ((1 << aggregate) - 1)][j++];
            bits >>= aggregate;
            s += w[bits & ((1 << aggregate) - 1)][j++];
            bits >>= aggregate;
            s += w[bits & ((1 << aggregate) - 1)][j++];
            bits >>= aggregate;
            s += w[bits & ((1 << aggregate) - 1)][j++];
            bits >>= aggregate;
        }
    }
    return s;
}

score_index_t<1> *accusation_scores(const codeword_t * const *X, const double *p, const double *y, const Collusion &coll, unsigned int m, unsigned int n) {

    Sideinfo si(m);
    unsigned int bestn = n;

    return accusation_scores(X, p, y, coll, si, m, n, bestn);
}

score_index_t<1> *accusation_scores(const codeword_t * const *X, const double *p, const double *y, const Collusion &coll, const Sideinfo &si, unsigned int m, unsigned int n, unsigned int &bestn) {

    if (verbose)
        fprintf(stderr, "%s: allocating %u Kbytes of memory\n", progname, n*sizeof(score_index_t<1>)/1024);

    score_index_t<1> *scores = new score_index_t<1>[n];

    if (verbose) fprintf(stderr, "%s: computing weights...\n", progname);
    double **W = coll.weights(p, y, si, m, 1);
    print_weights(W, m);

    double **w = aggregate_weights(W, m);

    if (!quiet) fprintf(stderr, "%s: computing %d (SI %d) accusation scores...\n", progname, n-si.size(), si.size());
  
    Timer timer;
    unsigned int l = 0;
    for (unsigned int i = 0; i < n; i++) {
    
        if (si.in(i)) continue;
    
        double s = 0.0;
        const codeword_t *Xi = X[i];
        
        for (unsigned int j = 0; j < m/aggregate; ) {
            codeword_t bits = *Xi++;
            for (unsigned int k = 0; k < codeword_bits/aggregate/4; k++) {
                s += w[bits & ((1 << aggregate) - 1)][j++];
                bits >>= aggregate;
                s += w[bits & ((1 << aggregate) - 1)][j++];
                bits >>= aggregate;
                s += w[bits & ((1 << aggregate) - 1)][j++];
                bits >>= aggregate;
                s += w[bits & ((1 << aggregate) - 1)][j++];
                bits >>= aggregate;
            }
        }

        scores[l].score = s;
        scores[l].index[0] = i;
        l++;
    }
    if (timing) printf("%s: scores %.3f sec\n", progname, timer());
    if (verbose) fprintf(stderr, "%s: %.3f sec\n", progname, timer());

    free_w(w);
    free_W(W); 

    sort_scores(scores, l, bestn); 

    return scores;
}

score_index_t<1> *accusation_scores(const codeword_t * const *X, const double *p, const double *y, const Collusion &coll, const score_index_t<1> *best_scores, Sideinfo &si, unsigned int m, unsigned int n, unsigned int bestn) {

    assert(bestn <= n);

    if (verbose)
        fprintf(stderr, "%s: allocating %u Kbytes of memory\n", progname, n*sizeof(score_index_t<1>)/1024);

    score_index_t<1> *scores = new score_index_t<1>[bestn];

    double **W = coll.weights(p, y, si, m, 1);
    print_weights(W, m);
    
    double **w = aggregate_weights(W, m);
  
    fprintf(stderr, "%s: computing %d (SI %d) accusation scores...\n", progname, bestn, si.size());

    Timer timer;
    for (unsigned int i = 0; i < bestn; i++) {
        double s = 0.0;
        unsigned int user_index = best_scores[i].index[0];
        const codeword_t *Xi = X[user_index];
        
        for (unsigned int j = 0; j < m/aggregate; ) {
            codeword_t bits = *Xi++;
            for (unsigned int k = 0; k < codeword_bits/aggregate/4; k++) {
                s += w[bits & ((1 << aggregate) - 1)][j++];
                bits >>= aggregate;
                s += w[bits & ((1 << aggregate) - 1)][j++];
                bits >>= aggregate;
                s += w[bits & ((1 << aggregate) - 1)][j++];
                bits >>= aggregate;
                s += w[bits & ((1 << aggregate) - 1)][j++];
                bits >>= aggregate;
            }
        }

        scores[i].score = s;
        scores[i].index[0] = user_index;
    }
    if (timing) printf("%s: scores %.3f sec\n", progname, timer());
    if (verbose) fprintf(stderr, "%s: scores %.3f sec\n", progname, timer());

    delete [] best_scores;

    free_w(w);
    free_W(W); 

    sort_scores(scores, bestn, bestn); 

    return scores;
}

