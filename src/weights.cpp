#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cassert>
#include <cstring>

#include "weights.h"
#include "fmath.hpp"
#include "debug.h"

double **ProbS_coll(const double *p, const Sideinfo &si, unsigned int m, unsigned int c, unsigned int t, unsigned int s) {
    assert(si.size() < c);
    assert(t <= c);
    assert(si.size() + t <= c);

    double **P = new double *[c+1];
    for (unsigned int i = 0; i <= c; i++)
        P[i] = new double[m];

    unsigned int l = si.size() + t;
    for (unsigned int i = 0; i < m; i++) {
        for (unsigned int j = 0; j <= c; j++) {
            unsigned int sk = j - si[i] - s;
            if (sk >= 0 && sk <= c - l) {
                unsigned long long nck = n_choose_k(c-l, sk);
                P[j][i] = nck * pow(p[i], sk) * pow(1.0 - p[i], c - l - sk);
            }
            else 
                P[j][i] = 0.0;
        }
    }

    return P;
}

void free_ProbS(double **ProbS, unsigned int c) {
    for (unsigned int i = 0; i <= c; i++)
        delete [] ProbS[i];
    delete [] ProbS;
}

void theta_ProbS(__m128 *PY, const float *theta, const __m128 * const *ProbS, unsigned int m, unsigned int c) {
    __m128 t = _mm_set1_ps(theta[0]); 
    for (unsigned int i = 0; i < m/4; i++) 
        PY[i] = t * ProbS[0][i];
    
    for (unsigned int i = 1; i <= c; i++) {
        __m128 t = _mm_set1_ps(theta[i]); 
        for (unsigned int j = 0; j < m/4; j++) 
            PY[j] += t * ProbS[i][j];
    }
}

double likelihood_theta(const double *theta, const double * const *ProbS, const double *y, unsigned int m, unsigned int c) {
    double PY[m];
    theta_ProbS(PY, theta, ProbS, m, c);
    
    double l = 0.0;
    for (unsigned int i = 0; i < m; i++) 
        l += (y[i] > 0.0) ? log(PY[i]) : log(1.0 - PY[i]);

    return l;
}

double likelihood_theta_approx(const double *theta, const double * const *ProbS, const double * y, unsigned int m, unsigned int c) {
    double PY[m];
    theta_ProbS(PY, theta, ProbS, m, c);
    
    double l = 0.0;
    for (unsigned int i = 0; i < m; i++) 
        l += (y[i] > 0.0) ? fmath::log(PY[i]) : fmath::log(1.0 - PY[i]);

    return l;
}

static inline float accum(__m128 &x) {
    return ((float *)&x)[0] + ((float *)&x)[1] + ((float *)&x)[2] + ((float *)&x)[3];
}

float likelihood_theta_approx(const float *theta, const __m128 * const *ProbS, unsigned int m, unsigned int c, unsigned int zeros) {
    // assume ProbS is aligned on 16 bytes
    __m128 PY[roundup(m, 4)/4];

    theta_ProbS(PY, theta, ProbS, m, c);

    __m128 l = _mm_setzero_ps();
    __m128 one = _mm_set1_ps(1.0f);
    unsigned int i = 0;
    while (i < zeros/4) {
        l += fmath::log_ps(one - PY[i++]);
    }

    static MIE_ALIGN(16) unsigned int mask0[4][4] = {
        {0, 0, 0, 0},
        {-1, 0, 0, 0},        
        {-1, -1, 0, 0},
        {-1, -1, -1, 0},
        };

    static MIE_ALIGN(16) unsigned int mask1[4][4] = {
        {-1, -1, -1, -1},
        {0, -1, -1, -1},        
        {0, 0, -1, -1},
        {0, 0, 0, -1},
        };
    
    l += _mm_and_ps(fmath::log_ps(one - PY[i]), *(__m128*)mask0[zeros & 3]);
    l += _mm_and_ps(fmath::log_ps(PY[i]), *(__m128*)mask1[zeros & 3]);
    i++;

    while (i < m/4) {
        l += fmath::log_ps(PY[i++]);
    }
       
    return accum(l);
}

float likelihood_theta_approx(const __m128 * PY, unsigned int m, unsigned int zeros) {
    __m128 l = _mm_setzero_ps();
    __m128 one = _mm_set1_ps(1.0f);
    unsigned int i = 0;
    while (i < zeros/4) {
        l += fmath::log_ps(one - PY[i++]);
    }

    static MIE_ALIGN(16) unsigned int mask0[4][4] = {
        {0, 0, 0, 0},
        {-1, 0, 0, 0},        
        {-1, -1, 0, 0},
        {-1, -1, -1, 0},
        };

    static MIE_ALIGN(16) unsigned int mask1[4][4] = {
        {-1, -1, -1, -1},
        {0, -1, -1, -1},        
        {0, 0, -1, -1},
        {0, 0, 0, -1},
        };
    
    l += _mm_and_ps(fmath::log_ps(one - PY[i]), *(__m128*)mask0[zeros & 3]);
    l += _mm_and_ps(fmath::log_ps(PY[i]), *(__m128*)mask1[zeros & 3]);
    i++;

    while (i < m/4) {
        l += fmath::log_ps(PY[i++]);
    }
       
    return accum(l);
}

void free_W(double **W, unsigned int t) {
    assert(t >= 1);
    
    for (unsigned int i = 0; i <= t; i++)
        delete [] W[i];

    delete [] W;
}

double **compute_weights_theta(const double *p, const double *y, const double *theta, const Sideinfo &si, unsigned int m, unsigned int c, unsigned int t) {

    double **W = new double*[t+1];
    for (unsigned int i = 0; i <= t; i++) {
        W[i] = new double[roundup(m, codeword_bits)];
        memset(W[i], 0, roundup(m, codeword_bits) * sizeof(double));
    }
    
    double **ProbS = ProbS_coll(p, si, m, c);

    double *logPY = new double[m];
    theta_ProbS(logPY, theta, ProbS, m, c);

    for (unsigned int i = 0; i < m; i++) {
        if (logPY[i] > 1.0) logPY[i] = 1.0;
        logPY[i] = log((y[i] > 0.0) ? logPY[i] : (1.0 - logPY[i]));
    }
    free_ProbS(ProbS, c);
    
    for (unsigned int k = 0; k <= t; k++) {
        double **ProbS = ProbS_coll(p, si, m, c, t, k);
        double *PY_X = new double[m];
        theta_ProbS(PY_X, theta, ProbS, m, c);

        for (unsigned int i = 0; i < m; i++) {
            if (std::isinf(logPY[i])) W[k][i] = -INFINITY;
            else {
                if (PY_X[i] > 1.0) PY_X[i] = 1.0;
                
                W[k][i] = log((y[i] > 0.0) ? PY_X[i] : (1.0-PY_X[i])) - logPY[i]; 
                W[k][i] /= (si.size()+t);
            }
            
            if (std::isnan(W[k][i])) {
                fprintf(stderr, "XXX %d %d %f %f %f %f %f %f\n", i, k, y[i], 
                    PY_X[i], logPY[i], W[k][i], log(PY_X[i]), log(PY_X[i]) - logPY[i]);
                fprintf(stderr, "%s: NaN in weights, must not happen, exit!\n", progname);
                exit(EXIT_FAILURE);
            }
        }        
        free_ProbS(ProbS, c);
        delete [] PY_X;
    }

    delete [] logPY;

    return W;
}

double **compute_weights_mu(const double *p, const double *y, const double *mu, const Sideinfo &si, unsigned int m, unsigned int c, unsigned int t, double awgn) {

    double **W = new double*[t+1];
    for (unsigned int i = 0; i <= t; i++) {
        W[i] = new double[roundup(m, codeword_bits)];
        memset(W[i], 0, roundup(m, codeword_bits) * sizeof(double));
    }
    
    double **ProbS = ProbS_coll(p, si, m, c);

    double *logPY = new double[m];
    mu_awgn_ProbS(logPY, mu, y, ProbS, m, c, awgn);
    for (unsigned int i = 0; i < m; i++) 
        logPY[i] = log(logPY[i]);
    free_ProbS(ProbS, c);
    
    for (unsigned int k = 0; k <= t; k++) {
        double **ProbS = ProbS_coll(p, si, m, c, t, k);
        double *PY_X = new double[m];
        mu_awgn_ProbS(PY_X, mu, y, ProbS, m, c, awgn);

        for (unsigned int i = 0; i < m; i++) {
            if (std::isinf(logPY[i])) W[k][i] = -INFINITY;
            else {
                W[k][i] = log(PY_X[i]) - logPY[i]; 
                W[k][i] /= (si.size()+t);
            }
            if (std::isnan(W[k][i])) {
                fprintf(stderr, "XXX %d %d %f %f %f %f %f %f\n", i, k, y[i], 
                    PY_X[i], logPY[i], W[k][i], log(PY_X[i]), log(PY_X[i]) - logPY[i]);
                fprintf(stderr, "%s: NaN in weights, must not happen, exit!\n", progname);
                exit(EXIT_FAILURE);
            }
        }        
        free_ProbS(ProbS, c);
        delete [] PY_X;
    }

    delete [] logPY;

    return W;
}

bool weights_inf(const double * const *W, unsigned int m, unsigned int t) {
    for (unsigned int i = 0; i < m; i++) {
        for (unsigned int j = 0; j <= t; j++) 
            if (std::isinf(W[j][i])) return true;
    }
    
    return false;
}

double **compute_weights_nu(const double *p, const double *y, const double *nu, const Sideinfo &si, unsigned int m, unsigned int c, unsigned int t, double awgn) {

    double **W = new double*[t+1];
    for (unsigned int i = 0; i <= t; i++) {
        W[i] = new double[roundup(m, codeword_bits)];
        memset(W[i], 0, roundup(m, codeword_bits) * sizeof(double));
    }
    
    double **ProbS = ProbS_coll(p, si, m, c);

    double *logPY = new double[m];
    nu_awgn_ProbS(logPY, nu, y, ProbS, m, c, awgn);
    for (unsigned int i = 0; i < m; i++) 
        logPY[i] = log(logPY[i]);
    free_ProbS(ProbS, c);
    
    for (unsigned int k = 0; k <= t; k++) {
        double **ProbS = ProbS_coll(p, si, m, c, t, k);
        double *PY_X = new double[m];
        nu_awgn_ProbS(PY_X, nu, y, ProbS, m, c, awgn);

        for (unsigned int i = 0; i < m; i++) {
            if (std::isinf(logPY[i])) W[k][i] = -INFINITY;
            else {
                W[k][i] = log(PY_X[i]) - logPY[i]; 
                W[k][i] /= (si.size()+t);
            }
            if (std::isnan(W[k][i])) {
                fprintf(stderr, "XXX %d %d %f %f %f %f %f %f\n", i, k, y[i], 
                    PY_X[i], logPY[i], W[k][i], log(PY_X[i]), log(PY_X[i]) - logPY[i]);
                fprintf(stderr, "%s: NaN in weights, must not happen, exit!\n", progname);
                exit(EXIT_FAILURE);
            }
        }        
        free_ProbS(ProbS, c);
        delete [] PY_X;
    }

    delete [] logPY;

    return W;
}

double likelihood_mu(const double *mu, const double * const *ProbS, const double *y, unsigned int m, unsigned int c, double awgn) {
    double PY[m];
    mu_awgn_ProbS(PY, mu, y, ProbS, m, c, awgn);

    double l = 0.0;
    for (unsigned int i = 0; i < m; i++) { 
        l += log(PY[i]);
    }
    
    return l;
}

double likelihood_mu_approx(const double *mu, const double * const *ProbS, const double *y, unsigned int m, unsigned int c, double awgn) {

    double l = 0.0;
    for (unsigned int i = 0; i < m; i++) { 
        double s = 0.0;
        for (unsigned int j = 0; j <= c; j++) {
            s += fmath::exp(-0.5 * sqr(y[i] - mu[j]) / awgn) * ProbS[j][i];
        }

        l += fmath::log(s / sqrt(2.0 * M_PI * awgn));
    }

    return l;
}

float likelihood_mu_approx(const float *mu, const __m128 * const *ProbS, const __m128 *y, unsigned int m, unsigned int c, float awgn) {
    __m128 l[m/4];

    const __m128 f = _mm_set1_ps(-0.5f / awgn);

    const __m128 mu4 = _mm_set1_ps(mu[0]);
    const __m128 *ps = ProbS[0];
    for (unsigned int i = 0; i < m/4; i++) { 
        __m128 tmp = y[i] - mu4;
        l[i] = fmath::exp_ps(tmp * tmp * f) * ps[i];
    }

    for (unsigned int j = 1; j <= c; j++) {
        const __m128 mu4 = _mm_set1_ps(mu[j]);
        const __m128 *ps = ProbS[j];
        for (unsigned int i = 0; i < m/4; i++) { 
            __m128 tmp = y[i] - mu4;
            l[i] += fmath::exp_ps(tmp * tmp * f) * ps[i];
        }
    }

    __m128 a = _mm_setzero_ps();
    const __m128 g = _mm_set1_ps(1.0f / sqrtf(2.0f * 3.1415926f * awgn));
    for (unsigned int i = 0; i < m/4; i++) 
        a += fmath::log_ps(l[i] * g);

    return accum(a);
}

double likelihood_nu(const double *nu, const double * const *ProbS, const double *y, unsigned int m, unsigned int c, double awgn) {
    double PY[m];
    nu_awgn_ProbS(PY, nu, y, ProbS, m, c, awgn);

    double l = 0.0;
    for (unsigned int i = 0; i < m; i++) { 
        l += log(PY[i]);
    }
    
    return l;
}

double likelihood_nu_approx(const double *nu, const double * const *ProbS, const double *y, unsigned int m, unsigned int c, double awgn) {
    double l = 0.0;
    for (unsigned int i = 0; i < m; i++) { 
        double s = 0.0;
        for (unsigned int j = 0; j <= c; j++) {
            double x = (nu[j] * fmath::exp(-0.5 * sqr(y[i] - 1.0) / awgn) + (1.0 - nu[j]) * fmath::exp(-0.5 * sqr(y[i] - -1.0) / awgn));
            s += x * ProbS[j][i];
        }

        l += fmath::log(s / sqrt(2.0 * M_PI * awgn));
    }
    
    return l;
}

float likelihood_nu_approx(const float *nu, const __m128 * const *ProbS, const __m128 *y, unsigned int m, unsigned int c, float awgn) {
    __m128 l[m/4];

    const __m128 f = _mm_set1_ps(-0.5f / awgn);
    const __m128 one4 = _mm_set1_ps(1.0f);

    const __m128 nu4 = _mm_set1_ps(nu[0]);
    const __m128 *ps = ProbS[0];
    for (unsigned int i = 0; i < m/4; i++) { 
        __m128 tmp1 = y[i] - one4;
        __m128 tmp2 = y[i] + one4;
        l[i] = (nu4 * fmath::exp_ps(tmp1 * tmp1 * f)  + (one4 - nu4) * fmath::exp_ps(tmp2 * tmp2 * f)) * ps[i];
    }

    for (unsigned int j = 1; j <= c; j++) {
        const __m128 nu4 = _mm_set1_ps(nu[j]);
        const __m128 *ps = ProbS[j];
        for (unsigned int i = 0; i < m/4; i++) { 
            __m128 tmp1 = y[i] - one4;
            __m128 tmp2 = y[i] + one4;
            l[i] += (nu4 * fmath::exp_ps(tmp1 * tmp1 * f)  + (one4 - nu4) * fmath::exp_ps(tmp2 * tmp2 * f)) * ps[i];
        }
    }

    __m128 a = _mm_setzero_ps();
    const __m128 g = _mm_set1_ps(1.0f / sqrtf(2.0f * 3.1415926f * awgn));
    for (unsigned int i = 0; i < m/4; i++) 
        a += fmath::log_ps(l[i] * g);

    return accum(a);
}

