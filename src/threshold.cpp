#include <cstring>
#include "threshold.h"
#include "rarev.h"
#include "prng.h"
#include "sideinfo.h"
#include "weights.h"
#include "debug.h"
#include "scores.h"
#include "scores_joint.h"
#include "generate.h"

struct rarev_t : public rarev_func_t<codeword_t> {
    rarev_t(const double *_p, const double *y, const Collusion &coll, const Sideinfo &si, unsigned int _m) : rarev_func_t<codeword_t>(prng_uint32()), p(_p), m(_m) {
        double **W = coll.weights(p, y, si, m, 1);
        w = aggregate_weights(W, m);
        free_W(W);        
    }

    ~rarev_t() {
        free_w(w);
    }

    void free(codeword_t *x) {
        free_sequence(x);
    }

    double score(const codeword_t *x) {
        double s = accusation_score(x, w, m);
        return s;
    }

    void copy(codeword_t *y, const codeword_t *x) {
        memcpy(y, x, roundup(m, codeword_bits)*sizeof(codeword_t)/codeword_bits);
    }

    void modify(codeword_t *y, const codeword_t *x, double mu) {
        memcpy(y, x, roundup(m, codeword_bits)*sizeof(codeword_t)/codeword_bits);

        int nrecomp = std::max((int) (m * mu), 1);
        unsigned int j = randi(m);

        while (nrecomp-- > 0) {
            double r = prng_double();
            codeword_t bit = (r < p[j]) ? 1 : 0;
            if (bit) y[j/codeword_bits] |= ((codeword_t)1 << (j % codeword_bits));
            else y[j/codeword_bits] &= ~((codeword_t)1 << (j % codeword_bits));
            if (++j >= m) {
                j = 0;
            }
        }
    }

    codeword_t *generate() {
        return generate_sequence(p, m);
    }
   
    const double *p;
    unsigned int m;
    double **w;
};

double scores_threshold(const double *p, const double *y, const Collusion &coll, const Sideinfo &si, unsigned int m, double pfa) {

    rarev_t rarev(p, y, coll, si, m);

    if (!quiet) fprintf(stderr, "%s: computing threshold (Pfa %e)...\n", progname, pfa);
    Timer timer;
    double thres = rarev_estimate_quantile<codeword_t>(pfa, rarev);
    timer.stop();
    if (timing) printf("%s: thresholding %.3f sec\n", progname, timer());
    if (verbose) fprintf(stderr, "%s: thresholding %.3f sec\n", progname, timer());

    return thres;
}

struct joint_rarev_t : public rarev_func_t<codeword_t *> {
    joint_rarev_t(const double *_p, const double *y, const Collusion &coll, const Sideinfo &si, unsigned int _m, unsigned int _t) : rarev_func_t<codeword_t *>(prng_uint32()), p(_p), m(_m), t(_t) {
        W = coll.weights(p, y, si, m, t);
    }

    ~joint_rarev_t() {
        free_W(W, t);
    }

    void free(codeword_t **x) {
        free_sequences(x, t);
    }

    double score(const codeword_t * const *x) {
        double s = accusation_joint_score(x, W, m, t);
        return s;
    }

    void copy(codeword_t **y, const codeword_t * const *x) {
        for (unsigned int k = 0; k < t; k++) 
            memcpy(y[k], x[k], roundup(m, codeword_bits)*sizeof(codeword_t)/codeword_bits);
    }

    void modify(codeword_t **y, const codeword_t * const *x, double mu) {
        for (unsigned int k = 0; k < t; k++) 
            memcpy(y[k], x[k], roundup(m, codeword_bits)*sizeof(codeword_t)/codeword_bits);

        for (unsigned int k = 0; k < t; k++) {
            unsigned int j = randi(m);
            int nrecomp = std::max((int) (m * mu), 1);

            while (nrecomp-- > 0) {
                double r = prng_double();
                codeword_t bit = (r < p[j]) ? 1 : 0;
                if (bit) y[k][j/codeword_bits] |= ((codeword_t)1 << (j % codeword_bits));
                else y[k][j/codeword_bits] &= ~((codeword_t)1 << (j % codeword_bits));
                if (++j >= m) j = 0;
            }
        }
    }

    codeword_t **generate() {
        return generate_sequences(p, m, t);
    }
   
    const double *p;
    double **W;
    unsigned int m;
    unsigned int t;
};

double joint_scores_threshold(const double *p, const double *y, const Collusion &coll, const Sideinfo &si, unsigned int m, unsigned int t, double pfa) {

    joint_rarev_t rarev(p, y, coll, si, m, t);

    if (!quiet) fprintf(stderr, "%s: computing %d-tuple threshold (Pfa %e)...\n", progname, t, pfa);
    Timer timer;
    double thres = rarev_estimate_quantile<codeword_t *>(pfa, rarev);
    timer.stop();
    if (timing) printf("%s: thresholding %.3f sec\n", progname, timer());
    if (verbose) fprintf(stderr, "%s: thresholding %.3f sec\n", progname, timer());

    return thres;
}

double tau(unsigned long long n, unsigned int c, double pfa) {
    return log((1.0/pfa - 1.0)*(n/float(c)-1.0));
}

double joint_scores_prob(const double *p, const double *y, const Collusion &coll, const Sideinfo &si, unsigned int m, unsigned int t, double score) {
    joint_rarev_t rarev(p, y, coll, si, m, t);

    fprintf(stderr, "%s: computing %d-tuple probability (> score %f)...\n", progname, t, score);
    Timer timer;
    double prob = rarev_estimate_threshold<codeword_t *>(score, rarev);
    timer.stop();
    if (timing) printf("%s: thresholding %.3f sec\n", progname, timer());
    if (verbose) fprintf(stderr, "%s: thresholding %.3f sec\n", progname, timer());

    return prob;
}
