#include <cstdio>
#include <cassert>
#include "sideinfo.h"

void Sideinfo::print(unsigned int o) const {
    if (debug) {
        fprintf(stderr, "%s: side info bits: ", progname);
        for (unsigned int i = 0; i < std::min(m, o); i++) {
            fprintf(stderr, "%d ", bits[i]);
        }
        fprintf(stderr, "\n");    
    }

    if (verbose) {
        fprintf(stderr, "%s: %d side info indices: ", progname, indices.size());
        for (std::set<unsigned int>::iterator it = indices.begin(); it != indices.end(); it++)
            fprintf(stderr, "%d ", *it);
        fprintf(stderr, "\n");    
    }
}

void Sideinfo::add_user(const codeword_t * const *X, unsigned int user) {
    assert(!in(user));
    indices.insert(user);
    dirty = true;

    // update sideinfo bits
    const codeword_t *Xi = X[user];
    for (unsigned int p = 0; p < m; p += codeword_bits) {
        codeword_t codeword = *Xi++;
        for (unsigned int b = 0; b < codeword_bits; b++) {
            bits[p+b] += codeword & 1;
            codeword >>= 1;
        }
    }
}

void Sideinfo::remove_user(const codeword_t * const *X, unsigned int user) {
    assert(in(user));
    indices.erase(user);
    dirty = true;

    // update sideinfo bits
    const codeword_t *Xi = X[user];
    for (unsigned int p = 0; p < m; p += codeword_bits) {
        codeword_t codeword = *Xi++;
        for (unsigned int b = 0; b < codeword_bits; b++) {
            bits[p+b] -= codeword & 1;
            codeword >>= 1;
        }
    }
}


void Sideinfo::add_user(const u8 *x, unsigned int user) {
    indices.insert(user);
    dirty = true;
    
    for (unsigned int i = 0; i < m; i++) 
        bits[i] += x[i];
}

bool Sideinfo::in(unsigned int user) const {
    // check if user index is in sideinfo
    std::set<unsigned int>::iterator seen = indices.find(user);
    return seen != indices.end();
}


