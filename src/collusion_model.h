#ifndef COLLUSION_MODEL_H
#define COLLUSION_MODEL_H

#include "collude.h"

typedef enum {unknown, soft, hard, sweet, choose} decoding_t;
const char *decoding_str(decoding_t mode);

class Collusion {
    public:
        const collusion_model_t collusion_model;
        const decoding_t decoding;
        const bool estimate;
        const bool marking_assumption;
    
        Collusion(collusion_model_t cm, decoding_t d, bool e, bool ma) : collusion_model(cm), decoding(d), estimate(e), marking_assumption(ma) {
            use_decoding = unknown;
            model_est = 0;
            model_params = 0;
            awgn_est = 0.0;
        }
        Collusion(const Collusion &o) : collusion_model(o.collusion_model), decoding(o.decoding), estimate(o.estimate), marking_assumption(o.marking_assumption) {
            use_decoding = unknown;
            model_est = 0;
            model_params = 0;
            awgn_est = 0.0;
        }
        ~Collusion() {
            if (model_est)
                delete [] model_est;
        }

        void init(const double *p, const double *y, Sideinfo &si, unsigned int m, unsigned int c, unsigned int d);
        void refine(const double *p, const double *y, Sideinfo &si, unsigned int m);

        double **weights(const double *p, const double *y, const Sideinfo &si, unsigned int m, unsigned int t) const;
        unsigned int c() const {
            assert(model_params > 0) ;
            return model_params - 1;
        }
        void print(unsigned int o = 10) const;

    private:
        unsigned int model_params;
        decoding_t use_decoding;
        double *model_est;
        double awgn_est;
        
        double *copy_model_est() const;
};

#endif

