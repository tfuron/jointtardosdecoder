#include <cassert>
#include <map>
#include <cmath>
#include <cstring>
#include "scores_joint.h"
#include "weights.h"
#include "debug.h"

static double load_user(const double * const *W, int *sbits, const codeword_t *X, unsigned int m) {
    double s = 0.0;

    for (unsigned int p = 0; p < m; p += codeword_bits) {
        codeword_t codeword = *X++;
        for (unsigned int b = 0; b < codeword_bits; b++) {
            *sbits += codeword & 1;
            s += W[*sbits++][p+b];
            codeword >>= 1;
        }
    }
    return s;
}

static void load_user(int *sbits, const codeword_t *X, unsigned int m) {
    for (unsigned int p = 0; p < m; p += codeword_bits) {
        codeword_t codeword = *X++;
        for (unsigned int b = 0; b < codeword_bits; b++) {
            *sbits++ += codeword & 1;
            codeword >>= 1;
        }
    }
}

static void unload_user(int *sbits, const codeword_t *X, unsigned int m) {
    for (unsigned int p = 0; p < m; p += codeword_bits) {
        codeword_t codeword = *X++;
        for (unsigned int b = 0; b < codeword_bits; b++) {
            *sbits++ -= codeword & 1;
            codeword >>= 1;
        }
    }
}

static double score_user(const double * const *W, const int *sbits, const codeword_t *X, unsigned int m) {
    double s = 0.0;

    for (unsigned int p = 0; p < m; p += codeword_bits) {
        codeword_t codeword = *X++;
        for (unsigned int b = 0; b < codeword_bits; b++) {
            s += W[*sbits++ + (codeword & 1)][p+b];
            codeword >>= 1;
        }
    }
    return s;
}

static double exchange_user(const double * const *W, int *sbits, const codeword_t *Xin, const codeword_t *Xout, unsigned int m) __attribute__ ((noinline));

static double exchange_user(const double * const *W, int *sbits, const codeword_t *Xin, const codeword_t *Xout, unsigned int m) {
    double s = 0.0;

    for (unsigned int p = 0; p < m; ) {
        codeword_t codeword_in = *Xin++;
        codeword_t codeword_out = *Xout++;

        for (unsigned int b = 0; b < codeword_bits; b += 8) {
            *sbits += (codeword_in & 1) - (codeword_out & 1);
            s += W[*sbits++][p++];
            codeword_out >>= 1;
            codeword_in >>= 1;            
            *sbits += (codeword_in & 1) - (codeword_out & 1);
            s += W[*sbits++][p++];
            codeword_out >>= 1;
            codeword_in >>= 1;            
            *sbits += (codeword_in & 1) - (codeword_out & 1);
            s += W[*sbits++][p++];
            codeword_out >>= 1;
            codeword_in >>= 1;            
            *sbits += (codeword_in & 1) - (codeword_out & 1);
            s += W[*sbits++][p++];
            codeword_out >>= 1;
            codeword_in >>= 1;            
            *sbits += (codeword_in & 1) - (codeword_out & 1);
            s += W[*sbits++][p++];
            codeword_out >>= 1;
            codeword_in >>= 1;            
            *sbits += (codeword_in & 1) - (codeword_out & 1);
            s += W[*sbits++][p++];
            codeword_out >>= 1;
            codeword_in >>= 1;            
            *sbits += (codeword_in & 1) - (codeword_out & 1);
            s += W[*sbits++][p++];
            codeword_out >>= 1;
            codeword_in >>= 1;
            *sbits += (codeword_in & 1) - (codeword_out & 1);
            s += W[*sbits++][p++];
            codeword_out >>= 1;
            codeword_in >>= 1;            
        }
    }
    return s;
}

static score_index_t<1> *revolving_door(const codeword_t * const *X, const double * const *W, const score_index_t<1> *scores, unsigned int m, unsigned int n, unsigned int t, unsigned int &bestn, double thres) {
    assert(t <= n);
    assert(bestn <= n);
    assert(t > 1);
    
    unsigned int combination[t+1];

    double *store = new double[bestn];
    for (unsigned int i = 0; i < bestn; i++) 
        store[i] = -INFINITY;

    unsigned int *userpower = new unsigned int[bestn];
    memset(userpower, 0, bestn*sizeof(unsigned int));
    
    int *sbits = new int[roundup(m, codeword_bits)];
    for (unsigned int i = 0; i < roundup(m, codeword_bits); i++) 
        sbits[i] = 0;
       
    // initialization
    double s = 0.0;
    for (unsigned int i = 0; i < t; i++) {
        combination[i] = i;
        s = load_user(W, sbits, X[scores[i].index[0]], m);
    }
    
    combination[t] = bestn;

    do {
        unsigned int j;

        // process combination
        for (unsigned int k = 0; k < t; k++) {
            if (s > thres) userpower[combination[k]]++;
            if (s > store[combination[k]]) store[combination[k]] = s;
        }
             
        if (t & 1) {
            // odd
            if (combination[0]+1 < combination[1]) {
                unload_user(sbits, X[scores[combination[0]].index[0]], m);
                for (unsigned int comb = combination[0]+1; comb < combination[1]; comb++) {
                    s = score_user(W, sbits, X[scores[comb].index[0]], m);

                    if (s > store[comb]) store[comb] = s;
                    if (s > thres) userpower[comb]++;
                    for (unsigned int k = 1; k < t; k++) {
                        if (s > store[combination[k]]) store[combination[k]] = s;
                        if (s > thres) userpower[combination[k]]++;                    
                    }
                }
                combination[0] = combination[1]-1;
                load_user(sbits, X[scores[combination[0]].index[0]], m);
            }
            j = 1;
        }
        else {
            // even
            if (combination[0] > 0) {
                unload_user(sbits, X[scores[combination[0]].index[0]], m);
                for (unsigned int comb = 0; comb < combination[0]; comb++) {

                    s = score_user(W, sbits, X[scores[comb].index[0]], m);

                    if (s > store[comb]) store[comb] = s;
                    if (s > thres) userpower[comb]++;
                    for (unsigned int k = 1; k < t; k++) {
                        if (s > store[combination[k]]) store[combination[k]] = s;
                        if (s > thres) userpower[combination[k]]++;                    
                    }
                }
                load_user(sbits, X[scores[0].index[0]], m);
                combination[0] = 0;
            }
            j = 1;
            goto r5;
        }
r4:
        if (combination[j] > j) {
            const codeword_t *Xout = X[scores[combination[j]].index[0]];
            combination[j] = combination[j-1];
            combination[j-1] = j-1;
            s = exchange_user(W, sbits, X[scores[combination[j-1]].index[0]], Xout, m);            
            continue;
        }
        else j++;
r5:
        if (combination[j]+1 < combination[j+1]) {
            const codeword_t *Xout = X[scores[combination[j-1]].index[0]];
            combination[j-1] = combination[j];
            combination[j]++;
            s = exchange_user(W, sbits, X[scores[combination[j]].index[0]], Xout, m);            
            continue;
        }
        else {
            j++;
            if (j < t) goto r4;
            break;
        }
        
    } while (true);

    delete [] sbits;

    std::multiset<user_info_t> best_info;
    unsigned int nvalid = 0;
    for (unsigned int i = 0; i < bestn; i++) {
        if (std::isinf(store[i])) continue;
        nvalid++;

        user_info_t user_info(store[i], userpower[i], scores[i].index[0]);
        best_info.insert(user_info);
    }
    bestn = nvalid;

    delete [] store;
    delete [] userpower;

    if (bestn == 0) 
        return 0;        

    score_index_t<1> *best_scores = new score_index_t<1>[bestn];
    unsigned int k = 0;
    for (std::multiset<user_info_t>::const_iterator it = best_info.begin(); it != best_info.end(); it++) {

        best_scores[k].score = it->score;
        best_scores[k].index[0] = it->user;
        best_scores[k].power = it->power;
        k++;
    }
  
    return best_scores;
}

score_index_t<1> *accusation_joint_scores(const codeword_t * const *X, const double *p, const double *y, const Collusion &coll, score_index_t<1> *scores, Sideinfo &si, unsigned int m, unsigned int n, unsigned int t, unsigned int &bestn, double thres) {

    assert(bestn <= n);

    double **W = coll.weights(p, y, si, m, t);
    print_weights(W, m, t);
    
    unsigned long long ntuples = n_choose_k(bestn, t);
    
    fprintf(stderr, "%s: computing %llu %d-tuple (SI %d) accusation scores / %d...\n", progname, ntuples, t, si.size(), bestn);

    Timer timer;
    score_index_t<1> *best_scores = revolving_door(X, W, scores, m, n, t, bestn, thres);
    timer.stop();
    if (timing) printf("%s: scores %.3f sec\n", progname, timer());
    if (verbose) fprintf(stderr, "%s: scores %.3f sec\n", progname, timer());

    free_W(W, t); 
    delete [] scores;

    return best_scores;
}

double accusation_joint_score(const codeword_t * const *X, const double * const *W, unsigned int m, unsigned int t) {
    double s = -INFINITY;
    int sbits[roundup(m, codeword_bits)];
    memset(sbits, 0, sizeof(sbits));
    for (unsigned int i = 0; i < t; i++) {
        s = load_user(W, sbits, X[i], m);
    }

    return s;
}
