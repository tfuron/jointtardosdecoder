#ifndef ACCUSE_H
#define ACCUSE_H

#include "common.h"
#include "sideinfo.h"

typedef enum {
    one,
    many,
} accusation_mode_t;

template <unsigned int t>
static score_index_t<1> *get_top_scores(const score_index_t<t> *best_scores, const Sideinfo &si, unsigned int bestn_in, unsigned int &bestn_out) {
    score_index_t<1> *scores = new score_index_t<1>[bestn_out];

    unsigned int k = 0;
    for (unsigned int i = 0; i < bestn_in; i++) {
        for (unsigned int j = 0; j < t; j++) {
            // check if user index is in sideinfo
            if (si.in(best_scores[i].index[j])) continue;

            // check if user is impossible
            if (std::isinf(best_scores[i].score)) continue;
            
            scores[k].score = best_scores[i].score;
            scores[k].index[0] = best_scores[i].index[j];
            k++;

            if (k >= bestn_out) goto done;
        }
    }

done:    
    // we got fewer scores than asked for, adjust bestn
    bestn_out = k;

    delete [] best_scores;

    return scores;
}

template <unsigned int t>
static unsigned int accuse(const codeword_t * const *X, const score_index_t<t> *scores, Sideinfo &si, unsigned int bestn, unsigned int c, double thres, unsigned int maxaccuse = -1) {
    unsigned int naccused = 0;
    for (unsigned int i = 0; i < bestn; i++) {

        if (scores[i].score > thres) {
            for (unsigned int j = 0; j < t; j++) {
                if (si.size() >= c || (naccused >= maxaccuse))
                    goto done;
    
                unsigned int accused_user = scores[i].index[j];
                if (verbose) fprintf(stderr, "%s: accusing %d (score %.06f)...\n", progname, accused_user, scores[i].score);
                si.add_user(X, accused_user);
                naccused++;
            }
        }
    }
    
done:
    if (verbose) fprintf(stderr, "%s: accused %d users\n", progname, naccused);
    return naccused;
}

const char *accusation_mode_str(accusation_mode_t mode);


#endif
