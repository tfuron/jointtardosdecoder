#ifndef GENERATE_H
#define GENERATE_H

#include "common.h"

double *generate_secret(unsigned int m, double t = 0.000167); // for c=20
double *generate_discrete_secret(unsigned int m, unsigned int c);
codeword_t *generate_sequence(const double *p, unsigned int m);
codeword_t **generate_sequences(const double *p, unsigned int m, unsigned int n);
void free_sequence(codeword_t *x);
void free_sequences(codeword_t **X, unsigned int n);

#endif
