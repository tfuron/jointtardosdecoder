#ifndef THRESHOLD_H
#define THRESHOLD_H

#include "common.h"
#include "sideinfo.h"
#include "collusion_model.h"

double scores_threshold(const double *p, const double *y, const Collusion &coll, const Sideinfo &si, unsigned int m, double pfa = 1e-4);

double joint_scores_threshold(const double *p, const double *y, const Collusion &coll, const Sideinfo &si, unsigned int m, unsigned int t, double pfa = 1e-4);

double joint_scores_prob(const double *p, const double *y, const Collusion &coll, const Sideinfo &si, unsigned int m, unsigned int t, double score);

double tau(unsigned long long n, unsigned int c, double pfa);

#endif

