#ifndef PRUNE_H
#define PRUNE_H

void prune100k(unsigned int &bestn_out, unsigned int t);
void prune250k(unsigned int &bestn_out, unsigned int t);
void prune500k(unsigned int &bestn_out, unsigned int t);
void prune1m(unsigned int &bestn_out, unsigned int t);
void prune2m(unsigned int &bestn_out, unsigned int t);
void prune5m(unsigned int &bestn_out, unsigned int t);
void prune10m(unsigned int &bestn_out, unsigned int t);
void prune25m(unsigned int &bestn_out, unsigned int t);
void prune50m(unsigned int &bestn_out, unsigned int t);
void prune100m(unsigned int &bestn_out, unsigned int t);
void prune250m(unsigned int &bestn_out, unsigned int t);
void prune1g(unsigned int &bestn_out, unsigned int t);

#endif
