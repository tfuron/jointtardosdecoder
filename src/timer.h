#ifndef TIMER_H
#define TIMER_H

#include <sys/time.h>

class Timer {
    public:
        Timer();
        void start();
        void stop();
        double operator()() const;
    private: 
        static double diff(const timeval &t1, const timeval &t0);
        timeval timer[2];
};

#endif /* TIMER_H */
