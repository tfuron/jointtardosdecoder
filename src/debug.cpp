#include <cstdio>
#include <algorithm>
#include "debug.h"

void print_weights(const double * const *W, unsigned int m, unsigned int t, unsigned int o) {
    if (!debug) return;

    for (unsigned int i = 0; i <= t; i++) {        
        fprintf(stderr, "%s: W%d = ", progname, i);
        for (unsigned int j = 0; j < std::min(o, m); j++)
            fprintf(stderr, "%.3f ", W[i][j]);
        fprintf(stderr, "\n");
    }
}

void print_collusion(const double *y, unsigned int m, decoding_t decoding, unsigned int o) {
    if (!debug) return;

    fprintf(stderr, "%s: y = ", progname);
    for (unsigned int i = 0; i < std::min(o, m); i++) {
        if (decoding == hard)
            fprintf(stderr, "%d ", (y[i] > 0.0) ? 1 : 0);
        else
            fprintf(stderr, "%.02f ", y[i]);
    }
    fprintf(stderr, "\n");
}

void print_secret(const double *p, unsigned int m, unsigned int o) {
    if (!debug) return;

    fprintf(stderr, "%s: p = ", progname);
    for (unsigned int i = 0; i < std::min(o, m); i++)
        fprintf(stderr, "%.6f ", p[i]);
    fprintf(stderr, "\n");
}

void print_users(const codeword_t * const *X, unsigned int m, unsigned int n, unsigned int p, unsigned int o) {
    if (!debug) return;

    for (unsigned int i = 0; i < std::min(p, n); i++) {
        fprintf(stderr, "%s: X[%u] = ", progname, i);
        for (unsigned int j = 0; j < std::min(o, m); j++)
            fprintf(stderr, "%lu ", (X[i][j / codeword_bits] >> (j % codeword_bits)) & 1);
        fprintf(stderr, "\n");
    }
}

void print_PY(const double *PY, unsigned int m, unsigned int o) {
    if (!debug) return;

    fprintf(stderr, "%s: PY = ", progname);
    for (unsigned int i = 0; i < std::min(o, m); i++)
        fprintf(stderr, "%.6f ", PY[i]);
    fprintf(stderr, "\n");
}

void print_ProbS(const double * const *ps, unsigned int m, unsigned int c, unsigned int o) {
    for (unsigned int i = 0; i <= c; i++) {
        fprintf(stderr, "%s: ProbS%d = ", progname, i);
        for (unsigned int j = 0; j < std::min(o, m); j++) 
            printf("%f ", ps[i][j]);
        printf("\n");
    }
}

