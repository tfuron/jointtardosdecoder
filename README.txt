Fingerprinting Source Code
Copyright (c) 2011 Peter Meerwald, Teddy Furon
and INRIA Rennes

version 0.3


This software packages provides C/C++ Source code to 
construct and decode fingerprinting (traitor tracing) 
codes, in particular Tardos codes.
It is meant for non-commercial research.


License
-------

The files in this package are under the CeCILL license, 
version 2 (see http://cecill.info). The license text can
be found in the file Licence_CeCILL_V2-en.txt.
CeCILL is a GPL variant written by CNRS/INRIA and confirming 
to French law.


Dependencies
------------

The following software components are used and have a 
different copyright and license:

SIMD-oriented Fast Mersenne Twister (SFMT):
http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/SFMT/index.html
Copyright (c) 2006,2007 Mutsuo Saito, Makoto Matsumoto and 
Hiroshima University.
Files are in directory dSFMT/, using version 2.1,
see dSFMT/LICENSE.txt.

fast approximate float function (fmath):
http://homepage1.nifty.com/herumi/soft/fmath.html
Copyright (c) Shigeo MITSUNARI
Files are in directory fmath/, using the version of 2010/Feb/16,
see fmath/LICENSE.txt.

rarev, a rare event simulation method translated
from MATLAB source code under the CeCILL license.
http://www.irisa.fr/texmex/people/furon/RareEvent_toolbox.zip
Files are in directory rarev/.


How-to compile / How-to use
---------------------------

A good place to learn how to use this software is the
file doc/Usage.txt. Instructions on how to build/compile
the software are provided in doc/Building.txt.


Publications
------------

If you find this software useful, it would be appreciated
if you could cite the following papers:

  * P. Meerwald, T. Furon, "Towards joint decoding of Tardos fingerprinting 
    codes", INRIA Bretagne Atlantique, Rennes, France, May 2011. Submitted to
    IEEE Trans. Information Forensics and Security, http://arxiv.org/abs/1104.5616.

  * P. Meerwald, T. Furon, "Iterative single Tardos decoder with 
    controlled probability of false positive", In Proc. of the 
    IEEE Int. Conf. on Multimedia & Expo, ICME '11, Barcelona, 
    Spain, July 2011.

  * P. Meerwald, T. Furon, "Towards Joint Tardos Decoding: The 
    'Don Quixote' Algorithm", In Proc. of the Information Hiding 
    Conf., IH ' 11, Prague, Czech Republic, LNCS, Springer, vol. 6958, 
    pp. 28 - 42,  May 2011.

  * P. Meerwald, T. Furon, "Group testing meets traitor tracing", 
    In Proc. of the IEEE Int. Conf. on Acoustics, Speech and Signal 
    Proc., ICASSP '11, Prague, Czech Republic, pp. 4204 - 4207, May 2011.


Contact
-------

Peter Meerwald (pmeerw@pmeerw.net)
Teddy Furon (teddy.furon@inria.fr)
