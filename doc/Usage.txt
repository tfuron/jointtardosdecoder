Usage / Program Options
=======================


The program tardosj offers an option -h to display
a brief summary of available options:

USAGE: tardos [OPTIONS]

  -n n		number of users / codewords
  -m n		code length (must be multiple of 32)
  -c n		colluder assumption (default: cmax)
          	can be: cmax, real, or a number     
  -b n		colluders to test (default: 2)
  -B n		bound of colluders (default: 10)
  -I n		increment for number of colluders (default: 1)
  -C name	collusion strategy (default: uniform)
		can be: uniform, majority, minority, allone, allzero,
		coinflip, wca-s, wca-j, averaging, minmax, rnd
  -M name	accusation mode (default: one)
  	  	can be: one, many
  -1		restrict to single decoder only, no joint decoding
  -j		disable joint decoding
  -e		estimate collusion model (default: known)
  -F n		bias distribution (default: 0)
		can be: 0 (Tardos), or 1 - 8 (discrete)
  -A n		AWGN (default: 0.0)
  -s n		pseudo-random number generator seed (default: 1234)
  -P n		target overall false-alarm rate (default: 1e-3)
  -t n		number of tests to perform per setting (default: 1)
  -v		turn verbose output on
  -d		turn debug output on
  -T		turn timing output on
  -h		display this help and exit
            					  		                  

Description
-----------

  -n n		n specifies the number of users / codewords to generate;
		note that all codewords are created in memory requiring
		n * m / 8 bytes -- use a 64-bit executable for large-scale
		setups
 
  -m n		n specifies the code length; it is recommended to use
		only multiples of 32 or 64 (depending on the CPU architecture);
		other code length may work but are not very well tested

  
  -c 'cmax' or 'real' or n
		this option fixes the strategy to determine the number of
		colluders assumed during decoding;

			cmax uses the upper bound given by the -B option (see
			below); this is the default and relates to the most 
			practical assumption

			real uses the current number of colluders going from
			the values given with the -b and -B options; this relates
			to a MAP decoder knowing the real number of colluders

			finally, an integer n can be used to set the number
			of colluders to a fixed value (use with care!)

  -b n1		these two options determine the range of colluders to test on the
  -B n2		code (n1 <= n2); defaults are 2 and 10 respectively

  -I n		is the increment used when testing the number of colluders going 
		from -b n1 to -B n2; default is 1

  -C name	determines the collusion strategy; the following processes can be 
		used and result in a binary forged codeword:

			uniform		uniform/interleaving collusion

			majority	majority vote

			minority	minority vote

			allone		all-one

			allzero		all-zero

			coinflip	coin-flipping

			wca-s		worst-case attack against a single decoder
					table based implementation for c=2..10

			wca-j		worst-case attack against a joint decoder
					table based implementation for c=2..9

		the following strategies violate marking assumption and result in
		a real valued forged codeword:

			averaging	sum-up codeword bits, divide by c

			minmax		-1 if k==0
					 1 if k==c
					 0 else

			rnd		-1 if k==0
					 1 if k==c
					 uniformly drawn in (-1,1) else

  -M 'one' or 'many'
		accusation mode: either accuse only one colluder (and then stop) or
		accuse as many colluders as possible; default is 'one'

  -1		restrict decoder to single decoding (no iterative or joint decoding)

  -j		disable joint decoding
		
  -e		enable estimation (or inference) of the collusion process; used for
		practical tracing scenarios, otherwise the collusion is known to the 
		decoder (MAP decoder)

  -F n		specify bias distribution for code construction; use Tardos distribution (n=0) 
		or a discrete distribution for a given number of colluders (n=1..8);
                default is: 0 (Tardos)

  -A n		add white Gaussian noise with variance n to the forged codeword; it is
		recommended to enable estimation (option '-e') if used; default is 0.0

  -s n		seed for the pseudo-random number generator seed (default: 1234)

  -P n		target overall false-alarm rate (default: 1e-3)

  -t n		number of tests to perform per setting (default: 1)
		useful to run several decoding experiments without regenerating all 
		the codewords

  -v		turn verbose output on

  -d		turn debug output on

  -T		turn timing output on

  -h		display this help and exit

